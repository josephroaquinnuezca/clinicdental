import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LoginComponent } from './admin-routes/login/login.component';
import { CreateAppointmentComponent } from './admin-routes/create-appointment/create-appointment.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule } from './shared/materials.module';
import { ViewAppointmentComponent } from './admin-routes/view-appointment/view-appointment.component';
// import { DashboardAppointmentComponent } from './admin-routes/dashboard-appointment/dashboard-appointment.component';
import { LogoutComponent } from './_reusesable-dialogs/logout/logout.component';
import { ProfileComponent } from './_reusesable-dialogs/profile/profile.component';
import { CreateAccountComponent } from './admin-routes/create-account/create-account.component';
import { ForgotPasswordComponent } from './admin-routes/forgot-password/forgot-password.component';
import { DocktorNavComponent } from './navigation/docktor-nav/docktor-nav.component';
import { ManageAppointmentComponent } from './doctor-routes/manage-appointment/manage-appointment.component';
import { ManagePatientsComponent } from './doctor-routes/manage-patients/manage-patients.component';
import { DoctorDashboardComponent } from './doctor-routes/doctor-dashboard/doctor-dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
// import { MapComponent } from './_reusesable-dialogs/map/map.component';
import { HttpClientModule } from '@angular/common/http';
import { StaffComponent } from './doctor-routes/staff/staff.component';

import { ToastrModule } from 'ngx-toastr';
import { AddDialogComponent } from './doctor-routes/manage-appointment/add-dialog/add-dialog.component';
import { ViewDialogComponent } from './doctor-routes/manage-appointment/view-dialog/view-dialog.component';
import { HomePageComponent } from './admin-routes/home-page/home-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeLandingPageComponent } from './static-routes/home-landing-page/home-landing-page.component';
import { ServiceLandingPageComponent } from './static-routes/service-landing-page/service-landing-page.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { ServiceDialogModule } from './_reusesable-dialogs/service-dialog/service-dialog.module';
// import { ErrorDialogComponent } from './_reusesable-dialogs/error-dialog/error-dialog.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AuthGuardGuard } from './_shared-core/auth-guard.guard';
import { ManageReportsComponent } from './doctor-routes/manage-reports/manage-reports.component';
import { EditDialogComponent } from './doctor-routes/manage-appointment/edit-dialog/edit-dialog.component';
import { NgxPrinterModule } from 'ngx-printer';
import { ManageClientScheduleComponent } from './doctor-routes/manage-client-schedule/manage-client-schedule.component';
import { ConfirmSchedDialogComponent } from './doctor-routes/manage-client-schedule/confirm-sched-dialog/confirm-sched-dialog.component';
import { EditViewAppointmentDialogComponent } from './admin-routes/view-appointment/edit-view-appointment-dialog/edit-view-appointment-dialog.component';
import { CalendarScheduleComponent } from './doctor-routes/calendar-schedule/calendar-schedule.component';
import { FullCalendarModule } from '@fullcalendar/angular';

//full calendar
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import { DatePipe } from '@angular/common';
import { ViewDataCalendarComponent } from './doctor-routes/calendar-schedule/view-data-calendar/view-data-calendar.component';
import { ViewPatientsDialogComponent } from './doctor-routes/manage-patients/view-patients-dialog/view-patients-dialog.component';
import { CommentDialogComponent } from './admin-routes/view-appointment/comment-dialog/comment-dialog.component';
import { UserGraphComponent } from './doctor-routes/manage-reports/user-graph/user-graph.component';
import { AppointmentGraphComponent } from './doctor-routes/manage-reports/appointment-graph/appointment-graph.component';
import { WalkinUserDialogComponent } from './doctor-routes/manage-client-schedule/walkin-user-dialog/walkin-user-dialog.component';
import { PatientWalkinDialogComponent } from './doctor-routes/manage-patients/patient-walkin-dialog/patient-walkin-dialog.component';
import { SetAppointmentWalkinDialogComponent } from './doctor-routes/manage-client-schedule/walkin-user-dialog/set-appointment-walkin-dialog/set-appointment-walkin-dialog.component';
import { CreateAppointmentDialogComponent } from './admin-routes/create-appointment-dialog/create-appointment-dialog.component';
import { ConfirmationDeleteDialogComponent } from './_resusesable-dialogs/confirmation-delete-dialog/confirmation-delete-dialog.component';
import { RejectPatientComponent } from './_reusesable-dialogs/reject-patient/reject-patient.component';
import { ConfirmPatientComponent } from './_reusesable-dialogs/confirm-patient/confirm-patient.component';
import { CalendarDialogComponent } from './_reusesable-dialogs/calendar-dialog/calendar-dialog.component';
import { AdminLoginComponent } from './doctor-routes/admin-login/admin-login.component';
import { HeaderToolbarComponent } from './_reusesable-dialogs/header-toolbar/header-toolbar.component';
import { BillingListComponent } from './billing/module/billing-list/billing-list.component';
import { ServicesListComponent } from './services/module/services-list/services-list.component';
import { ServicesAddDialogComponent } from './services/module/dialog/services-add-dialog/services-add-dialog.component';
import { BillingAddDialogComponent } from './billing/module/dialog/billing-add-dialog/billing-add-dialog.component';
import { TransactionHistoryComponent } from './transaction/module/transaction-history/transaction-history.component';
import { ServicesEditDialogComponent } from './services/module/dialog/services-edit-dialog/services-edit-dialog.component';
import { ServicesDeleteDialogComponent } from './services/module/dialog/services-delete-dialog/services-delete-dialog.component';
import { AddNoteTeethDialogComponent } from './_reusesable-dialogs/add-note-teeth-dialog/add-note-teeth-dialog.component';
import { ToothHistoryPageComponent } from './admin-routes/tooth-history-page/tooth-history-page.component';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NavigationComponent } from './school-property/navigation/navigation.component';
import { CustodianComponent } from './school-property/custodian/custodian.component';
import { BuildingComponent } from './school-property/building/building.component';
import { LaboratoryComponent } from './school-property/laboratory/laboratory.component';
import { RoomsComponent } from './school-property/rooms/rooms.component';
import { CreateCustodianComponent } from './school-property/custodian/create-custodian/create-custodian.component';
import { EditCustodianComponent } from './school-property/custodian/edit-custodian/edit-custodian.component';
import { DeleteCustodianComponent } from './school-property/custodian/delete-custodian/delete-custodian.component';
import { CreateRoomsComponent } from './school-property/rooms/create-rooms/create-rooms.component';
import { EditRoomsComponent } from './school-property/rooms/edit-rooms/edit-rooms.component';
import { DeleteRoomsComponent } from './school-property/rooms/delete-rooms/delete-rooms.component';
import { UsersAccountComponent } from './school-property/users-account/users-account.component';
import { UsersCreateComponent } from './school-property/users-account/users-create/users-create.component';
import { UsersEditComponent } from './school-property/users-account/users-edit/users-edit.component';
import { StockInComponent } from './school-property/custodian/stock-in/stock-in.component';
import { StockOutComponent } from './school-property/custodian/stock-out/stock-out.component';
import { SchoolReportsComponent } from './school-property/school-reports/school-reports.component';
import { SchoolHistoryComponent } from './school-property/school-history/school-history.component';
import { SchoolDashboardComponent } from './school-property/school-dashboard/school-dashboard.component';
import { CreaeBuildingComponent } from './school-property/building/creae-building/creae-building.component';
import { CreaeLaboratoryComponent } from './school-property/laboratory/creae-laboratory/creae-laboratory.component';
import { BorrowItemsComponent } from './school-property/laboratory/borrow-items/borrow-items.component';
import { BorrowItemListComponent } from './school-property/borrow-item-list/borrow-item-list.component';
import { UpdateLaboratoryComponent } from './school-property/laboratory/update-laboratory/update-laboratory.component';
import { ShowRoomListComponent } from './school-property/rooms/show-room-list/show-room-list.component';
import { CreateRoomItemsComponent } from './school-property/rooms/show-room-list/create-room-items/create-room-items.component';
import { authInterceptorProviders } from './_shared-core/auth.interceptor';
import { DeleteBuildingComponent } from './school-property/building/delete-building/delete-building.component';
import { EditBuildingComponent } from './school-property/building/edit-building/edit-building.component';
import { ShowBuildingListComponent } from './school-property/building/show-building-list/show-building-list.component';
import { CreateBuildingListComponent } from './school-property/building/show-building-list/create-building-list/create-building-list.component';
import { ItemDialogUpdateComponent } from './school-property/shared-dialog/item-dialog-update/item-dialog-update.component';
import { ItemDialogDeleteComponent } from './school-property/shared-dialog/item-dialog-delete/item-dialog-delete.component';
import { StocksMgntComponent } from './school-property/custodian/stocks-mgnt/stocks-mgnt.component';
import { StockoutMngtComponent } from './school-property/custodian/stocks-mgnt/stockout-mngt/stockout-mngt.component';
import { BorrowItemBuildingComponent } from './school-property/building/show-building-list/borrow-item-building/borrow-item-building.component';
import { ReturnItemListComponent } from './school-property/borrow-item-list/return-item-list/return-item-list.component';
import { UpdateRoomItemsComponent } from './school-property/building/show-building-list/update-room-items/update-room-items.component';
import { BorrowItemLaboratoryComponent } from './school-property/laboratory/borrow-item-laboratory/borrow-item-laboratory.component';
import { BorrowItemRoomsComponent } from './school-property/rooms/show-room-list/borrow-item-rooms/borrow-item-rooms.component';
import { EditRoomItemsComponent } from './school-property/rooms/show-room-list/edit-room-items/edit-room-items.component';
import { DeleteLaboratoryComponent } from './school-property/laboratory/delete-laboratory/delete-laboratory.component';
import { DeleteRoomListComponent } from './school-property/rooms/show-room-list/delete-room-list/delete-room-list.component';
import { DeleteItemBuildingComponent } from './school-property/building/show-building-list/delete-item-building/delete-item-building.component';
import { ViewListStockOutComponent } from './school-property/custodian/view-list-stock-out/view-list-stock-out.component';
import { StockinMngtComponent } from './school-property/custodian/stocks-mgnt/stockin-mngt/stockin-mngt.component';
import { ErrorDialogComponent } from './_reusesable-dialogs/error-dialog/error-dialog.component';
import { StatusManagementListComponent } from './school-property/status-management-list/status-management-list.component';
import { HomepagePropertyComponent } from './school-property/homepage-property/homepage-property.component';

// FullCalendarModule.registerPlugins([interactionPlugin, dayGridPlugin]);

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    LoginComponent,
    CreateAppointmentComponent,
    ViewAppointmentComponent,
    LogoutComponent,
    ProfileComponent,
    CreateAccountComponent,
    ForgotPasswordComponent,
    DocktorNavComponent,
    ManageAppointmentComponent,
    ManagePatientsComponent,
    DoctorDashboardComponent,
    StaffComponent,
    AddDialogComponent,
    ViewDialogComponent,
    HomePageComponent,
    LandingPageComponent,
    HomeLandingPageComponent,
    ServiceLandingPageComponent,
    ManageReportsComponent,
    EditDialogComponent,
    ManageClientScheduleComponent,
    ConfirmSchedDialogComponent,
    EditViewAppointmentDialogComponent,
    CalendarScheduleComponent,
    ViewDataCalendarComponent,
    ViewPatientsDialogComponent,
    CommentDialogComponent,
    UserGraphComponent,
    AppointmentGraphComponent,
    WalkinUserDialogComponent,
    PatientWalkinDialogComponent,
    SetAppointmentWalkinDialogComponent,
    CreateAppointmentDialogComponent,
    ConfirmationDeleteDialogComponent,
    RejectPatientComponent,
    ConfirmPatientComponent,
    CalendarDialogComponent,
    AdminLoginComponent,
    HeaderToolbarComponent,
    BillingListComponent,
    ServicesListComponent,
    ServicesAddDialogComponent,
    BillingAddDialogComponent,
    TransactionHistoryComponent,
    ServicesEditDialogComponent,
    ServicesDeleteDialogComponent,
    AddNoteTeethDialogComponent,
    ToothHistoryPageComponent,
    NavigationComponent,
    CustodianComponent,
    BuildingComponent,
    LaboratoryComponent,
    RoomsComponent,
    CreateCustodianComponent,
    EditCustodianComponent,
    DeleteCustodianComponent,
    CreateRoomsComponent,
    EditRoomsComponent,
    DeleteRoomsComponent,
    UsersAccountComponent,
    UsersCreateComponent,
    UsersEditComponent,
    StockInComponent,
    StockOutComponent,
    SchoolReportsComponent,
    SchoolHistoryComponent,
    SchoolDashboardComponent,
    CreaeBuildingComponent,
    CreaeLaboratoryComponent,
    BorrowItemsComponent,
    BorrowItemListComponent,
    UpdateLaboratoryComponent,
    ShowRoomListComponent,
    CreateRoomItemsComponent,
    DeleteBuildingComponent,
    EditBuildingComponent,
    ShowBuildingListComponent,
    CreateBuildingListComponent,
    ItemDialogUpdateComponent,
    ItemDialogDeleteComponent,
    StocksMgntComponent,
    StockinMngtComponent,
    StockoutMngtComponent,
    BorrowItemBuildingComponent,
    ReturnItemListComponent,
    UpdateRoomItemsComponent,
    BorrowItemLaboratoryComponent,
    BorrowItemRoomsComponent,
    EditRoomItemsComponent,
    DeleteLaboratoryComponent,
    DeleteRoomListComponent,
    DeleteItemBuildingComponent,
    ViewListStockOutComponent,
    ErrorDialogComponent,
    StatusManagementListComponent,
    HomepagePropertyComponent,
  ],
  imports: [
    NgxMaterialTimepickerModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
    }),
    FlexLayoutModule,
    NgxPrinterModule.forRoot({ printOpenWindow: true }),
    FullCalendarModule,
    MatDatepickerModule,
    BrowserModule,
    AppRoutingModule,
    MaterialsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule, //new
    FlexLayoutModule,
    // ServiceDialogModule,
    NgHttpLoaderModule.forRoot(),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [AuthGuardGuard, DatePipe, authInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}

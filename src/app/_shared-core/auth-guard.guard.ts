import { TokenStorageService } from './token-storage.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';





@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(
    private tokenValidate: TokenStorageService,
    private _router: Router,
  ){
  }

  canActivate(){

    const token = this.tokenValidate.getToken();

    // console.log("token", token)

    // const decoded_token = this.tokenValidate.decodeToken

    // console.log("DECODED TOKEN", decoded_token)

    if(!token){
      this._router.navigate([''])
      return false
    }else{
      return true
    }

  }

}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
// import {
//   ErrorDialogComponent,
//   ErrorReusesableDialogsService,
// } from '../_reusesable-dialogs/service-dialog';
import { catchError, finalize } from 'rxjs/operators';
import { TokenStorageService } from './token-storage.service';
import { Router } from '@angular/router';
import { LoginServiceService } from '../_shared/shared/login-service.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../_reusesable-dialogs/error-dialog/error-dialog.component';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private token: TokenStorageService,
    private router: Router,
    private toastr: ToastrService,
    private dialogRef: MatDialog,
    private userService: LoginServiceService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authReq = req;
    const token = this.token.getToken();

    if (token) {
      authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
      });
    }

    authReq = req.clone({
      withCredentials: true,
    });

    return next.handle(authReq).pipe(
      catchError((error, caught) => {
        console.log(error);
        console.log(caught);
        if (error instanceof HttpErrorResponse) {
          console.log(error.status);
          if ([403, 415].includes(error.status)) {
            const dialogRef = this.dialogRef.open(ErrorDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Your session is expired. Please signin again.',
            });
            dialogRef.afterClosed().subscribe((result) => {
              if (localStorage.getItem('dataSource')) {
                this.dialogRef.closeAll();
                this.userService.logout();
                this.router.navigate(['/admin/signin']);
              }
            });
          }
        }
        // this.handleError(error)
        return throwError(error);
      }),
      finalize(() => {
        // this.reusableDialogsService.closeDialogLoading();
      })
    ) as Observable<HttpEvent<any>>;
  }
}

export const authInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
];

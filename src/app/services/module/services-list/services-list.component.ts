import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { ServicesAddDialogComponent } from '../dialog/services-add-dialog/services-add-dialog.component';
import { ServicesDeleteDialogComponent } from '../dialog/services-delete-dialog/services-delete-dialog.component';
import { ServicesEditDialogComponent } from '../dialog/services-edit-dialog/services-edit-dialog.component';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.scss']
})
export class ServicesListComponent implements OnInit {

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  dataSourceAll: MatTableDataSource<ClientInterface>;
  getAllData: any = [];

  displayedColumnsAll: string[] = [
    'number',
    'name',
    'date',
    'time',
    'category',
    'user_status',
    'conttrols',
  ];

  constructor(
    public dialog: MatDialog,
    public ClientServiceService: ClientServiceService
  ) {}

  ngOnInit() {
    this.getAllClientAppointmentAdmin();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllClientAppointmentAdmin();
  }

  getAllClientAppointmentAdmin() {
    this.ClientServiceService
      .getAllService(this.paginate)
      .subscribe((data) => {
        console.log(data);

        var result: any = data;

        this.getAllData = result.data.users;
        this.paginate.totalCount = result.data.totalCount;
        this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
        this.dataSourceAll.data = this.getAllData;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllClientAppointmentAdmin()
      : this.paginator.firstPage();
  }

  addServices() {
    const dialogRef = this.dialog.open(ServicesAddDialogComponent, {

    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClientAppointmentAdmin()
    });
  }

  delete(id) {
    const dialogRef = this.dialog.open(ServicesDeleteDialogComponent, {

    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClientAppointmentAdmin()
    });
  }

  edit(id, name, description, price) {
    const dialogRef = this.dialog.open(ServicesEditDialogComponent, {
      
        data: {
          id: id,
          name: name,
          description: description,
          price: price,

        },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClientAppointmentAdmin()
    });
  }

}

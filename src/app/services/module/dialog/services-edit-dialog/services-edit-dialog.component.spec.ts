import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesEditDialogComponent } from './services-edit-dialog.component';

describe('ServicesEditDialogComponent', () => {
  let component: ServicesEditDialogComponent;
  let fixture: ComponentFixture<ServicesEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicesEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-services-edit-dialog',
  templateUrl: './services-edit-dialog.component.html',
  styleUrls: ['./services-edit-dialog.component.scss']
})
export class ServicesEditDialogComponent implements OnInit {

  dialogdata: any
  createform: FormGroup;

  constructor(
    private router: Router,
    public clientService: ClientServiceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    public dialogRef: MatDialogRef<ServicesEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public results,
    ) {
      this.dialogdata = results
    }

  ngOnInit(): void {

    this.CreateFormValidation()
  }

  CreateFormValidation() {
    this.createform = this.formgroup.group(
      {
        service_name: [this.dialogdata.name, Validators.required],
        noOfteeth: [''],
        description: [this.dialogdata.description, Validators.required],
        price: [this.dialogdata.price, Validators.required],

      }
    );
  }

  get f() {
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;

    } else {
      // const date = moment(this.myDate).format('l');

      const dataClient = {
        service_name: this.createform.get('service_name').value,
        no_teeth: this.createform.get('noOfteeth').value,
        description: this.createform.get('description').value,
        service_price: this.createform.get('price').value

      };

      this.clientService.updateService(dataClient, this.dialogdata.id).subscribe(
        (data) => {
          console.log(data);
          this.dialogRef.close();
          this.toastr.success('Successfully Registered');
          // this.router.navigate(['/login']);
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );

      console.log('USER ACCOUNT', dataClient);
    }
  }

}

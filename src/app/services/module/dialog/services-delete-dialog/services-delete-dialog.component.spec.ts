import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesDeleteDialogComponent } from './services-delete-dialog.component';

describe('ServicesDeleteDialogComponent', () => {
  let component: ServicesDeleteDialogComponent;
  let fixture: ComponentFixture<ServicesDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicesDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

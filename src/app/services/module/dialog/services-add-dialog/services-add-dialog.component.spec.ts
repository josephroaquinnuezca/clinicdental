import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesAddDialogComponent } from './services-add-dialog.component';

describe('ServicesAddDialogComponent', () => {
  let component: ServicesAddDialogComponent;
  let fixture: ComponentFixture<ServicesAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicesAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

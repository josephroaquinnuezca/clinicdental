import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { LogoutComponent } from 'src/app/_reusesable-dialogs/logout/logout.component';
import { ProfileComponent } from 'src/app/_reusesable-dialogs/profile/profile.component';

@Component({
  selector: 'app-docktor-nav',
  templateUrl: './docktor-nav.component.html',
  styleUrls: ['./docktor-nav.component.scss'],
})
export class DocktorNavComponent implements OnInit {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  values: any

  constructor(public dialog: MatDialog, private observer: BreakpointObserver) {}

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }

  ngOnInit(): void {
    this.values = JSON.parse(localStorage.getItem('dataSource'));

  }

  openDialogLogout(): void {
    const dialogRef = this.dialog.open(LogoutComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }

  openProfileDialog(): void {
    const dialogRef = this.dialog.open(ProfileComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }
}

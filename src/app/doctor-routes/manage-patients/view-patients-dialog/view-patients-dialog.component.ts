import { HttpErrorResponse, JsonpInterceptor } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import * as moment from 'moment';
import { AddNoteTeethDialogComponent } from 'src/app/_reusesable-dialogs/add-note-teeth-dialog/add-note-teeth-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-view-patients-dialog',
  templateUrl: './view-patients-dialog.component.html',
  styleUrls: ['./view-patients-dialog.component.scss'],
})
export class ViewPatientsDialogComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  dataSourceAll: MatTableDataSource<any>;
  getAllData: any = [];
  getAllService: any = [];

  displayedColumnsAll: string[] = [
    'number',
    'name',
    'service',
    'total',
    'date',
    'user_status',
    'conttrols',
  ];

  paymentForm: FormGroup;
  signupform: FormGroup;

  hide = true;
  submitted = false;

  dialogData: any;

  DataValues: any;

  noTeeth: any;
  teethPrice: any;
  teethTotal: any;

  ServiceValues: any = [];

  getSelectedData: any;

  // paginate: any = {
  //   totalCount: 1100,
  //   pageSize: 10,
  //   page: 0,
  //   searchString: '',
  // };

  array_data: any = [];

  sumTotal: any = '';

  teethno: any = [
    { imagesrc: '', value: 1 },
    { imagesrc: '', value: 2 },
    { imagesrc: '', value: 3 },
    { imagesrc: '', value: 4 },
    { imagesrc: '', value: 5 },
    { imagesrc: '', value: 6 },
    { imagesrc: '', value: 7 },
    { imagesrc: '', value: 8 },
    { imagesrc: '', value: 9 },
    { imagesrc: '', value: 10 },
    { imagesrc: '', value: 11 },
    { imagesrc: '', value: 12 },
    { imagesrc: '', value: 13 },
    { imagesrc: '', value: 14 },
    { imagesrc: '', value: 15 },
    { imagesrc: '', value: 16 },
    { imagesrc: '', value: 17 },
    { imagesrc: '', value: 18 },
    { imagesrc: '', value: 19 },
    { imagesrc: '', value: 20 },
    { imagesrc: '', value: 21 },
    { imagesrc: '', value: 22 },
    { imagesrc: '', value: 23 },
    { imagesrc: '', value: 24 },
    { imagesrc: '', value: 25 },
    { imagesrc: '', value: 26 },
    { imagesrc: '', value: 27 },
    { imagesrc: '', value: 28 },
    { imagesrc: '', value: 29 },
    { imagesrc: '', value: 30 },
    { imagesrc: '', value: 31 },
    { imagesrc: '', value: 32 },
  ];

  //reg ex mobile patten phil
  mobNumberPattern = '^((\\+91-?)|0)?[0-9]{10}$';

  constructor(
    public dialogRef: MatDialogRef<ViewPatientsDialogComponent>,
    public dialog: MatDialog,
    private formgroup: FormBuilder,
    public clientService: ClientServiceService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {

    this.getSingleTransactionHistory();
    this.getAllServices();

    this.clientService.getSingleClient(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data;

      this.signupform.get('fname').setValue(this.DataValues?.fname);
      this.paymentForm.get('name').setValue(this.DataValues?.fname);

      console.log('results', result);

      // this.getAllClient();
    });

    //form validation using reactive form
    this.signupform = this.formgroup.group(
      {
        fname: ['', Validators.required],
        mname: ['', Validators.required],
        lname: ['', Validators.required],
        dob: ['', Validators.required],
        gender: ['', Validators.required],
        username: ['', Validators.required],
        password: [''],
        retypepassword: [''],
        mobile: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        address: ['', Validators.required],
      },
      { validator: this.checkIfMatchingPasswords('password', 'retypepassword') }
    );

    this.paymentForm = this.formgroup.group({
      service_id: [''],
      no_teeth: [''],
      price: [''],
      total: [''],
      service_name: [''],
      name: [''],
    });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  addnote(id) {
    const dialogRef = this.dialog.open(AddNoteTeethDialogComponent, {
      data: {
        id: id,
        client_id:this.dialogData.id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }

  //this button remove element from an array
  removeElement(i: number){
    this.array_data.splice(i, 1);
  }

  //check the password match
  checkIfMatchingPasswords(
    passwordKey: string,
    passwordConfirmationKey: string
  ) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  get f() {
    return this.signupform.controls;
  }

  onSubmit() {
    if (this.signupform.invalid) {
      this.signupform.markAllAsTouched();

      this.toastr.error('Please Complete all required fields!');

      return;
    } else {
      var values = JSON.parse(localStorage.getItem('dataSource'));

      const age = moment().diff(this.signupform.get('dob').value, 'years'); //age

      const create = {
        fname: this.signupform.get('fname').value,
        mname: this.signupform.get('mname').value,
        lname: this.signupform.get('lname').value,
        dob: this.signupform.get('dob').value,
        gender: this.signupform.get('gender').value,
        username: this.signupform.get('username').value,
        password: this.signupform.get('password').value,
        contact_no: this.signupform.get('mobile').value,
        age: age,
        email: this.signupform.get('email').value,
        address: this.signupform.get('address').value,
      };

      this.clientService.updateClient(create, values.id).subscribe(
        (data) => {
          console.log(data);
          this.dialogRef.close();
          window.sessionStorage.clear();
          window.location.reload();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );

      console.log('CREATE CLIENT', create);
    }
  }

  getAllServices() {
    this.clientService.getAllService(this.paginate).subscribe((data) => {
      console.log(data);

      var result: any = data;

      console.log('Result', result.data.users);

      this.ServiceValues = result.data.users;

      // this.getAllData = result.data.users;
      // this.paginate.totalCount = result.data.totalCount;
      // this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
      // this.dataSourceAll.data = this.getAllData;

      // this.dataSourceAll.data = this.getAllData;
    });
  }

  selectService(event) {
    console.log('event value', event.value);

    this.clientService.getSingleService(event.value).subscribe(
      (data) => {
        console.log('Get Single data', data);

        console.log('getsingle service', data);

        this.getSelectedData = data.data;

        this.paymentForm
          .get('service_name')
          .setValue(this.getSelectedData?.service_name);
        this.paymentForm
          .get('price')
          .setValue(this.getSelectedData?.service_price);

        // this.paymentForm.get('price').reset()
        this.paymentForm.get('total').reset();
        this.paymentForm.get('no_teeth').reset();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }

  teethChange(event, price) {
    console.log(price);

    //  const available_stock = this.medicineform.get('med_available').value

    //  console.log(available_stock)

    //

    //  if( event.target.value >= quantity){
    //   this.toastr.error('The quantity is greater than the available stocks');
    //   //  this._snackBar.open("The quantity is greater than the available stocks");
    //  }else{

    // this.toastr.success('Good');
    const result = event.target.value * price;

    this.noTeeth = event.target.value;
    this.teethTotal = result;

    this.paymentForm.get('total').patchValue(result);

    //  }
  }

  sum(obj) {
    var sum = 0;
    for (var el in obj) {
      if (obj.hasOwnProperty(el)) {
        sum += parseFloat(obj[el]);
      }
    }
    return sum;
  }

  onSave() {
    if (this.paymentForm.invalid) {
      //this is to show all the required fields

      // this.medicineform.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {
      this.array_data.push({
        service_id: this.paymentForm.get('service_id').value,
        noTeeth: this.paymentForm.get('no_teeth').value,
        total: this.paymentForm.get('total').value,
        service_name: this.paymentForm.get('service_name').value,
        price_service: this.paymentForm.get('price').value,
      });

      console.log(this.array_data);

      this.sumTotal = this.array_data.reduce(
        (accum, item) => accum + item.total,
        0
      );

      // const sumValues = Object.values(this.array_data).reduce((total) => +parseFloat(obj[key]||0), 0);

      console.log(this.sumTotal);
    }
  }

  onSubmitTransaction() {
    const data = [
      {
        client_id: this.dialogData.id,
        service: JSON.stringify(this.array_data),
        sum_total: this.sumTotal,
      },
    ];

    console.log('array data', data);
    this.clientService.createTransaction(data).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success('Successfully Inserted');
        this.dialogRef.close();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }

  //Transaction History
  // onChangedPage(page: PageEvent) {
  //   console.log('PAGE EVENT', page);
  //   this.paginate.page = page.pageIndex;
  //   this.paginate.pageSize = page.pageSize;
  //   // console.log("PAGE LANG", page)
  //   // this.getAllTransaction();
  // }

  getSingleTransactionHistory() {
    this.clientService.getSingleTransactionHistory(this.paginate, this.dialogData.id).subscribe((data) => {


      console.log("getAllTransaction",this.dialogData.id);

      var result: any = data;

      console.log("Data",  result.data.users)

      // console.log("element",  JSON.parse(result.data.users.) )

      // result.data.users.forEach(element => {
      //   console.log("element", element.service)
      //   this.getAllService = element.service
      // });


      this.getAllData = result.data.users;
      // this.paginate.totalCount = result.data.totalCount;
      this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
      this.dataSourceAll.data = this.getAllData;

      // this.dataSourceAll.data = this.getAllData;
    });
  }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this.paginate.searchString = filterValue;
  //   console.log('view search string', filterValue);

  //   this.paginator.pageIndex == 0
  //     ? this.getAllTransaction()
  //     : this.paginator.firstPage();
  // }
}

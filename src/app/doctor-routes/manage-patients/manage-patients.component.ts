import { ViewPatientsDialogComponent } from './view-patients-dialog/view-patients-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatPaginator,
  MatPaginatorIntl,
  PageEvent,
} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { MatDialog } from '@angular/material/dialog';
import { PatientWalkinDialogComponent } from './patient-walkin-dialog/patient-walkin-dialog.component';
import { ProfileComponent } from 'src/app/_reusesable-dialogs/profile/profile.component';
import { RejectPatientComponent } from 'src/app/_reusesable-dialogs/reject-patient/reject-patient.component';
import { ConfirmPatientComponent } from 'src/app/_reusesable-dialogs/confirm-patient/confirm-patient.component';

@Component({
  selector: 'app-manage-patients',
  templateUrl: './manage-patients.component.html',
  styleUrls: ['./manage-patients.component.scss'],
})
export class ManagePatientsComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  getAllData: any = [];

  dataSourceAll: MatTableDataSource<ClientInterface>;
  displayedColumnsAll: string[] = [
    'number',
    'name',
    'contact',
    'address',
    'status',
    'date',
    'conttrols',
  ];

  // 'gender',

  // dataSource: MatTableDataSource<any>;

  constructor(
    public dialog: MatDialog,
    private clientService: ClientServiceService,
    public _MatPaginatorIntl: MatPaginatorIntl //rename the item per page
  ) {}

  ngOnInit() {
    this._MatPaginatorIntl.itemsPerPageLabel = 'Display Number:';

    // FUNCTION GET ALL CLIENT
    this.getAllClient();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllClient();
  }

  getAllClient() {
    this.clientService.getAllClient(this.paginate).subscribe((data) => {
      console.log('data data', data);

      var result: any = data;

      this.getAllData = result.data.users;

      this.paginate.totalCount = result.data.totalCount;
      this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
      this.dataSourceAll.data = this.getAllData;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllClient()
      : this.paginator.firstPage();
  }

  confirm(id) {
    this.clientService.ConfirmedClient(id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.getAllClient();
    });
  }

  opendelete(id) {
    this.clientService.DeleteClient(id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.getAllClient();
    });
  }


 openConfirmDialog(id) {
    const dialogRef = this.dialog.open(ConfirmPatientComponent, {
      data: {
        id: id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClient();
    });
  }


  openRejectDialog(id) {
    const dialogRef = this.dialog.open(RejectPatientComponent, {
      data: {
        id: id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClient();
    });
  }

  viewpatient(id) {
    const dialogRef = this.dialog.open(ViewPatientsDialogComponent, {
      data: {
        id: id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }

  addwalkin() {
    const dialogRef = this.dialog.open(PatientWalkinDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      // this.getAllClientAppointmentAdmin();
      this.getAllClient();
    });
  }

  editpatient(id) {
    const dialogRef = this.dialog.open(PatientWalkinDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      // this.getAllClientAppointmentAdmin();
    });
  }
} //CLOSING TAG


// openReject(id) {
//   this.clientService.RejectClient(id).subscribe((data) => {
//     console.log(data);
//     console.log(id);
//     var result: any = data;

//     this.getAllClient();
//   });
// }

// private iterator() {
//   const end = (this.currentPage + 1) * this.pageSize;
//   const start = this.currentPage * this.pageSize;
//   const part = this.array.slice(start, end);
//   this.dataSourceAll = part;
// }

// public handlePage(e: any) {
//   this.currentPage = e.pageIndex;
//   this.pageSize = e.pageSize;
//   this.iterator();
// }

// this.clientService.getAllClient(this.paginate)
// .subscribe((data) => {
//   console.log(data)
//   var result: any = data

//   this.getAllData = result.data

//   this.dataSourceAll = new MatTableDataSource<ClientInterface>(this.getAllData);
//   // this.dataSourceAll.paginator = this.paginator;
//   // this.array = this.getAllData;
//   // this.totalSize = this.array.length;
//   // this.iterator();

//   this.dataSourceAll.data = this.getAllData;

// });

// this.dataSource = new MatTableDataSource<any>(ELEMENT_DATA);
// this.dataSource.paginator = this.paginator;
// this.array = ELEMENT_DATA;
// this.totalSize = this.array.length;
// this.iterator();

// this.dataSource.data = ELEMENT_DATA;

// this.getAllData = result.data

// this.dataSourceAll = new MatTableDataSource<ClientInterface>(this.getAllData);
// this.dataSourceAll.paginator = this.paginator;
// this.array = this.getAllData;
// this.totalSize = this.array.length;
// this.iterator();

// this.array = this.getAllData;
// this.totalSize = this.array.length;
// this.iterator();

// this.dataSourceAll.data = this.getAllData;
// public array: any;
// public pageSize = 5;
// public currentPage = 0;
// public totalSize = 0;

import { Component, OnInit } from '@angular/core';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import * as moment from 'moment'; // add this 1 of 4
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'node_modules/chart.js';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.scss'],
})
export class DoctorDashboardComponent implements OnInit {
  dataValues: any = [];

  dataAppointment: any = [];

  dataChart: any = [];

  currentyear: any = [];

  myDate = new Date(Date.now());

  constructor(
    private ClientService: ClientServiceService,
    public appointMentService: AppointServiceService,
    ) {
    Chart.register(
      ArcElement,
      LineElement,
      BarElement,
      PointElement,
      BarController,
      BubbleController,
      DoughnutController,
      LineController,
      PieController,
      PolarAreaController,
      RadarController,
      ScatterController,
      CategoryScale,
      LinearScale,
      LogarithmicScale,
      RadialLinearScale,
      TimeScale,
      TimeSeriesScale,
      Decimation,
      Filler,
      Legend,
      Title,
      Tooltip,
      SubTitle
    );
  }

  ngOnInit() {

    this.currentyear = new Date().getFullYear()


    const mydateme = moment(this.myDate).format('l');

    this.appointMentService.GetAllClientAppointmentToday(mydateme).subscribe((data) => {
      console.log(data);

      const result: any = data.data[0];
      this.dataAppointment = result;

      console.log(result);
    });


    this.getAllcount();
    this.chartFunction();
    this.pieChartFunction();
  }

  getAllcount() {
    this.ClientService.getalllCount().subscribe((data) => {
      console.log(data);

      const result: any = data.data[0];
      this.dataValues = result;

      this.dataChart = data;

      console.log(result);
    });
  }

  chartFunction() {
    var ctx = document.getElementById('myChart');

    this.ClientService.getalllCount().subscribe((data) => {

   const result: any = data.data[0];


    console.log('data one ', result);

    var myChart = new Chart('myChart', {
      type: 'bar',
      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        datasets: [
          {
            label: '',
            data: [
              result.January,
              result.February,
              result.March,
              result.April,
              result.May,
              result.June,
              result.July,
              result.August,
              result.September,
              result.October,
              result.November,
              result.December
            ],
            backgroundColor: [
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(255, 99, 132, 0.2)',
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });

  }); //end

  }

  pieChartFunction(){

    const DATA_COUNT = 5;
    const NUMBER_CFG = {count: DATA_COUNT, min: 0, max: 100};

    var ctx = document.getElementById('piechart');

    this.ClientService.getalllCount().subscribe((data) => {

      const result: any = data.data[0];

      var myChart = new Chart('piechart', {
        type: 'pie',
        data: {
          labels: [
            'Tooth Extraction', 'Fillings and Repair', 'Braces', 'Root Canal', 'Teeth Whitening'
          ],
          datasets: [
            {
              label: 'Dataset 1',
              data: [
                result.count_tooth_extraction,
                result.count_fillings_repair,
                result.count_braces,
                result.count_root_canal,
                result.count_teeth_whitening,
              ],
              backgroundColor: [
                '#05445E',
                '#189AB4',
                '#75E6DA',
                '#D4F1F4',
                '#18A558',
              ],
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Services'
            }
          }
        },
      });




  }); //end


  } //end function



}

import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-set-appointment-walkin-dialog',
  templateUrl: './set-appointment-walkin-dialog.component.html',
  styleUrls: ['./set-appointment-walkin-dialog.component.scss'],
})
export class SetAppointmentWalkinDialogComponent implements OnInit {
  defaultValue = { hour: 13, minute: 30 };

  disabledSubmitButton = false

  dateErrorMessage = false

  timeChangeHandler(event: Event) {
    console.log(event);
  }

  invalidInputHandler() {
    // some error handling
  }
  createform: FormGroup;
  show: boolean = false;
  hide: boolean = false;

  splitarrayTime: any;
  splitarrayCategory: any;

  storedAppointment: any = [];

  timeArray: any;
  categoryArray: any;

  subscription;

  testArray: any = [];

  dataSource: any = [];

  // myDate = Date.now();

  todayISOString: string = new Date().toISOString();

  myDate = new Date();

  dialogdata: any;

  DataValues: any;

  noTeeth: any;
  teethPrice: any;
  teethTotal: any;

  ServiceValues: any = [];

  getSelectedData: any;

  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  constructor(
    public dialogRef: MatDialogRef<SetAppointmentWalkinDialogComponent>,
    private datePipe: DatePipe,
    private router: Router,
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    public clientService: ClientServiceService,
    private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogdata = results;
  }

  ngOnInit() {
    this.getAllServices();

    console.log('ID', this.dialogdata.id);

    this.myDate.setDate(this.myDate.getDate());

    var values = JSON.parse(localStorage.getItem('dataSource'));
    this.dataSource = values;

    const time = new Date().getTime();
    console.log('time', time);

    const mydateme = moment(this.myDate).format('l');
    console.log('datemuko', mydateme);

    // this.getAllAppoint();

    this.createform = this.formgroup.group({
      appointmentdate: ['', Validators.required],
      appointmenttime: ['', Validators.required],
      category: ['', Validators.required],
    });
  }

  maxTimeValidator = (control: FormControl): { [key: string]: boolean } | null => {
    const selectedTime = control.value;
    const maxTime = new Date();
    maxTime.setHours(17, 0, 0); // Set the maximum time to 5 PM (17:00)

    if (selectedTime && selectedTime > maxTime) {
      return { maxTime: true };
    }

    return null;
  }

  getAllServices() {
    this.clientService.getAllService(this.paginate).subscribe((data) => {
      console.log(data);

      var result: any = data;

      console.log('Result', result.data.users);

      this.ServiceValues = result.data.users;
    });
  }

  timechange(event){

    const timeInputValue = event

    console.log("date change", event)


    if(timeInputValue >= "12:00" || timeInputValue == "01:00" ){
      this.disabledSubmitButton= true
    }

    // this.appointMentService
    //    .appointmentTimeValidation(timeInputValue, this.dialogdata.id)
    //    .subscribe(
    //      (data) => {
    //        console.log(data);
    //        this.toastr.success('The appointment time that you selected is available');
    //        this.disabledSubmitButton= false
    //        // this.dialogRef.close();
    //      },(err) => {
    //        if (err instanceof HttpErrorResponse) {
    //          console.log(err.status);
    //          console.log('hello', err.error.description);
    //          this.toastr.error(err.error.description);
    //          this.disabledSubmitButton = true
    //         //  this.createform.controls.appointmentdate.setErrors({invalidDate:true})
    //        } else {
    //          console.log(err);
    //        }
    //      },
    //      () => {
    //        console.log('request completed');
    //      }
    //    );
  }

  getKeys(obj) {
    return Object.keys(obj);
  }

  dateChange(event){
     // Access the value of the input using event.target.value
     const dateinputvalue = moment(event.target.value).format('l');

     console.log("date change", dateinputvalue)

     this.appointMentService
        .appointentDateValidation(dateinputvalue, this.dialogdata.id)
        .subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('The appointment that you selected is available');
            this.disabledSubmitButton= false
            // this.dialogRef.close();
          },(err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
              this.disabledSubmitButton = true
              this.createform.controls.appointmentdate.setErrors({invalidDate:true})
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );

  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please complete all required fields!!');
      return;
    } else {
      var values = JSON.parse(localStorage.getItem('dataSource'));

      // const modifytime = moment(
      //   this.createform.get('appointmenttime').value
      // ).format('LTS');
      const modifydate = this.createform.get('appointmentdate').value;

      // console.log("time",  modifytime)



      const CreateAppointment = {
        client_id: this.dialogdata.id,
        appointment_date: moment(
          this.createform.get('appointmentdate').value
        ).format('l'),
        appointment_time: this.createform.get('appointmenttime').value,
        appointment_category: JSON.stringify(
          this.createform.get('category').value
        ),
        month: modifydate.toLocaleString('en-us', { month: 'long' }),
        // year: this.createform.get('category').value,
      };


      // console.log("CreateAppointment", CreateAppointment)

      this.appointMentService
        .createClientAppointment(CreateAppointment)
        .subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('Successfully Created');
            this.dialogRef.close();
          },(err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );
    }
  } //onsubmit


}

// onChangeEvent(event: any) {
//   // const datechangeValue = moment(event.value).format('l');
//   const datechangeValue = moment(event.target.value).format('l');

//   let value = {
//     datechangeValue: datechangeValue,
//   };

//   console.log(value);
//   this.appointMentService.getSingleAppointment(value).subscribe(
//     (data) => {
//       this.storedAppointment = data;
//       this.timeArray = this.storedAppointment.data.appointment_time;
//       this.categoryArray = this.storedAppointment.data.appointment_category;

//       const arrayStringTime = this.timeArray;
//       const arrayStringCategory = this.categoryArray;

//       this.splitarrayTime = arrayStringTime.split(',');
//       this.splitarrayCategory = arrayStringCategory.split(',');

//       console.log('data Stored', this.storedAppointment.data);
//     },
//     (err) => {
//       if (err instanceof HttpErrorResponse) {
//         console.log(err.status);
//         console.log('hello', err.error.description);
//         this.toastr.error(err.error.description);
//         this.splitarrayCategory = [];
//         this.splitarrayTime = [];
//         this.createform.get('category').setValue('');
//         this.createform.get('appointmenttime').setValue('');
//       } else {
//         console.log(err);
//       }
//     },
//     () => {
//       console.log('request completed');
//     }
//   );
// }

// this.getAllData = result.data.users;
// this.paginate.totalCount = result.data.totalCount;
// this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
// this.dataSourceAll.data = this.getAllData;

// this.dataSourceAll.data = this.getAllData;

// const arrayStrings =  this.createform.get('category').value

// const arrayObjectCategory =   arrayStrings.map(String);

// console.log(arrayObjectCategory);

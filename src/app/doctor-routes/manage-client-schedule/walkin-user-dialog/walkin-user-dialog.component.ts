import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  MatPaginator,
  MatPaginatorIntl,
  PageEvent,
} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { MatSort } from '@angular/material/sort';
import { SetAppointmentWalkinDialogComponent } from './set-appointment-walkin-dialog/set-appointment-walkin-dialog.component';
import { CreateAppointmentComponent } from 'src/app/admin-routes/create-appointment/create-appointment.component';
import { CreateAppointmentDialogComponent } from 'src/app/admin-routes/create-appointment-dialog/create-appointment-dialog.component';

@Component({
  selector: 'app-walkin-user-dialog',
  templateUrl: './walkin-user-dialog.component.html',
  styleUrls: ['./walkin-user-dialog.component.scss'],
})
export class WalkinUserDialogComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  getAllData: any = [];

  dataSourceAll: MatTableDataSource<ClientInterface>;
  displayedColumnsAll: string[] = [
    'number',
    'name',
    'contact',
    'address',
    'status',
    'date',
    'conttrols',
  ];

  // 'gender',

  // dataSource: MatTableDataSource<any>;

  constructor(
    public dialog: MatDialog,
    private clientService: ClientServiceService,
    public _MatPaginatorIntl: MatPaginatorIntl //rename the item per page
  ) {}

  ngOnInit() {
    this._MatPaginatorIntl.itemsPerPageLabel = 'Display Number:';

    // FUNCTION GET ALL CLIENT
    this.getAllClient();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllClient();
  }

  getAllClient() {
    this.clientService.getAllClient(this.paginate).subscribe((data) => {
      console.log('data data', data);

      var result: any = data;

      this.getAllData = result.data.users;

      this.paginate.totalCount = result.data.totalCount;
      this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
      this.dataSourceAll.data = this.getAllData;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllClient()
      : this.paginator.firstPage();
  }

  setAppointment(id) {
    console.log(id);
    const dialogRef = this.dialog.open(SetAppointmentWalkinDialogComponent, {
      data: {
        id,
      },  
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClient();
    });
  }
}

// opendelete(id) {
//   this.clientService.DeleteClient(id).subscribe((data) => {
//     console.log(data);

//     var result: any = data;

//     this.getAllClient();
//   });
// }

// openReject(id) {
//   this.clientService.RejectClient(id).subscribe(data => {
//     console.log(data);
//     console.log(id)
//     var result: any = data;

//     // this.getAllClient();
//   });
// }

// viewpatient(id){
//   const dialogRef = this.dialog.open(ViewPatientsDialogComponent, {
//     data: {
//        id: id,
//        },
//   });

//   dialogRef.afterClosed().subscribe((result) => {
//     console.log(result);
//   });
// }

// addwalkin() {
//   const dialogRef = this.dialog.open(PatientWalkinDialogComponent, {
//     panelClass: 'app-full-bleed-dialog-p-10',
//     // data: {
//     //   id,
//     // }
//   });

//   dialogRef.afterClosed().subscribe((result) => {
//     console.log('The dialog was closed', result);
//     // this.getAllClientAppointmentAdmin();
//   });
// }

import { ConfirmSchedDialogComponent } from './confirm-sched-dialog/confirm-sched-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Appointment } from 'src/app/_interface/appointment.model';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { WalkinUserDialogComponent } from './walkin-user-dialog/walkin-user-dialog.component';
import { ConfirmationDeleteDialogComponent } from 'src/app/_resusesable-dialogs/confirmation-delete-dialog/confirmation-delete-dialog.component';
import { EditDialogComponent } from '../manage-appointment/edit-dialog/edit-dialog.component';

@Component({
  selector: 'app-manage-client-schedule',
  templateUrl: './manage-client-schedule.component.html',
  styleUrls: ['./manage-client-schedule.component.scss'],
})
export class ManageClientScheduleComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  dataSourceAll: MatTableDataSource<ClientInterface>;
  getAllData: any = [];

  displayedColumnsAll: string[] = [
    'number',
    'name',
    'date',
    'time',
    'category',
    'user_status',
    'conttrols',
  ];

  constructor(
    public dialog: MatDialog,
    public appointMentService: AppointServiceService
  ) {}

  ngOnInit() {
    this.getAllClientAppointmentAdmin();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllClientAppointmentAdmin();
  }

  getAllClientAppointmentAdmin() {
    this.appointMentService
      .getSingleClientAppointmentAdmin(this.paginate)
      .subscribe((data) => {
        console.log(data);

        var result: any = data;

        this.getAllData = result.data.users;
        this.paginate.totalCount = result.data.totalCount;
        this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
        this.dataSourceAll.data = this.getAllData;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllClientAppointmentAdmin()
      : this.paginator.firstPage();
  }

  openConfirmDialog(id, clientid, date, time, category) {
    const dialogRef = this.dialog.open(ConfirmSchedDialogComponent, {
      data: {
        id: id,
        clientid: clientid,
        date: date,
        time:time,
        category:category,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllClientAppointmentAdmin();
    });
  }

  // openReject(id, contactno) {
  //   this.appointMentService
  //     .rejectAppointmentClient(id, contactno)
  //     .subscribe((data) => {
  //       console.log(data);
  //       const result: any = data;
  //       this.getAllClientAppointmentAdmin();
  //     });
  // }


  openReject(id, contactno) {
    const dialogRef = this.dialog.open(ConfirmationDeleteDialogComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        id,
        contactno,
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClientAppointmentAdmin();
    });
  }

  addwalkin() {
    const dialogRef = this.dialog.open(WalkinUserDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClientAppointmentAdmin();
    });
  }

  editAppointment(id, clientid, date, time, category) {
    console.log('id', id);
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {
        id,
        clientid: clientid,
        date: date,
        time:time,
        category:category,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClientAppointmentAdmin();
    });
  }


}

// public handlePage(e: any) {
//   this.currentPage = e.pageIndex;
//   this.pageSize = e.pageSize;
//   this.iterator();
// }

// private iterator() {
//   const end = (this.currentPage + 1) * this.pageSize;
//   const start = this.currentPage * this.pageSize;
//   const part = this.array.slice(start, end);
//   this.dataSourceAll = part;
// }

// applyFilter(filterValue: string) {
//   filterValue = filterValue.trim();
//   filterValue = filterValue.toLowerCase();
//   this.dataSourceAll.filter = filterValue;
// }

// this.appointMentService
//   .getSingleClientAppointmentAdmin()
//   .subscribe((data) => {
//     console.log(data);

//     var result: any = data;

//     this.getAllData = result.data;
//     this.dataSourceAll = new MatTableDataSource<ClientInterface>(this.getAllData);

//     this.dataSourceAll.data = this.getAllData;

//   });

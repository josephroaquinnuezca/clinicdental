import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-confirm-sched-dialog',
  templateUrl: './confirm-sched-dialog.component.html',
  styleUrls: ['./confirm-sched-dialog.component.scss']
})
export class ConfirmSchedDialogComponent implements OnInit {

  dialogdata: any;

  constructor(
    public dialogRef: MatDialogRef<ConfirmSchedDialogComponent>,
    public appointMentService: AppointServiceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogdata = results
  }

  ngOnInit(): void {

  }

  confirm(){

    const data = {
      id: this.dialogdata.id,
      clientid: this.dialogdata.clientid,
    }

    console.log("data", data)

    this.appointMentService.UpdateSingleWaitingClient(data).subscribe(data =>{
      console.log(data);
      const result: any = data
      this.dialogRef.close();

    });
  }

}

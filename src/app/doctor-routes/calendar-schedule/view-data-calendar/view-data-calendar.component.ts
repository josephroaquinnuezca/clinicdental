import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-view-data-calendar',
  templateUrl: './view-data-calendar.component.html',
  styleUrls: ['./view-data-calendar.component.scss']
})
export class ViewDataCalendarComponent implements OnInit {

  dialogdata: any;

  dataSource: any = [];

  constructor(
    public clientService: ClientServiceService,
   @Inject(MAT_DIALOG_DATA) public results  ) {
    this.dialogdata = results;
  }

  ngOnInit(): void {
    this.GetSingleUsersAppointment();
  }

  GetSingleUsersAppointment() {
    var id = this.dialogdata.id;

    this.clientService
    .getSingleClient(this.dialogdata.id)
    .subscribe(data => {
      console.log(data);

      var result: any = data

      console.log("res", result.data)

      this.dataSource = result.data;


      // this.createform.get('appointment_date').setValue(result.data.appointment_date);
      // this.createform.get('appointment_time').setValue(result.data.appointment_time);
      // this.createform.get('appointment_category').setValue(result.data.appointment_category);


    });


  }

}

import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CalendarOptions, EventClickArg } from '@fullcalendar/angular';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { AddDialogComponent } from '../manage-appointment/add-dialog/add-dialog.component';
import { ViewDataCalendarComponent } from './view-data-calendar/view-data-calendar.component';

@Component({
  selector: 'app-calendar-schedule',
  templateUrl: './calendar-schedule.component.html',
  styleUrls: ['./calendar-schedule.component.scss'],
})
export class CalendarScheduleComponent implements OnInit {

  Events: any = [];
  calendarOptions: CalendarOptions;

  dataAll: any = [];

  datedata: any = [];
  titledata: any = [];

  constructor(
    public dialog: MatDialog,
    public datepipe: DatePipe,
    private appointMentService: AppointServiceService
  ) {}

  ngOnInit(): void {




    this.fetchAllClientAppointment();


    // console.log("eventsinside", this.calendarOptions.events)
  }

  fetchAllClientAppointment() {
    this.appointMentService.fetchmecalendar().subscribe((data) => {
      console.log(data);

      const result: any = data;

      this.dataAll = result.data;

      console.log('dummy data', this.dataAll.appointment_date);

      for (let dateonly of this.dataAll) {

        var datec = this.datepipe.transform(
          dateonly.appointment_date,
          'yyyy-MM-dd'
        );

        var userstats = dateonly.user_status
        var category = dateonly.appointment_category
        var id = dateonly.client_id



        var title =  dateonly.appointment_time

        // hour === 5? 'red': (hour === 7 ? 'green' : 'black');

        this.Events.push(
           {
              id: id,
              title: title + '-' + category,
              date: datec,
              color: userstats === 'Waiting' ? '#E1C340' : userstats === 'Rejected' ? '#F51720' : (userstats === 'Scheduled' ? '#317140' : 'black')
            }
        )



      }

      this.calendarOptions = {
        initialView: 'dayGridMonth',
        // eventColor: '#378006',
        // dateClick: this.onDateClick.bind(this),
        events:  this.Events,
        eventClick: this.handleEventClick.bind(this)
      }


      console.log('dateshet', this.datedata);
      console.log('formatdate', this.titledata);

      console.log('events', this.Events);

      // let accountTypeName:string[]=[];
      // this.dataAll.data.array.forEach(accountTypeItem => {
      //   accountTypeName.push(accountTypeItem.appointment_date);
      // });

      // console.log("fetch", accountTypeName)

      // const convert_date = this.datepipe.transform(
      //   result.data.appointment_date,
      //   'yyyy-MM-dd'
      // );

      // console.log('date', convert_date);
    });
  }

  handleEventClick(clickInfo: EventClickArg) {
    console.log(clickInfo)
    console.log(clickInfo.event.id)
    console.log(clickInfo.event.title)
    const dialogRef = this.dialog.open(ViewDataCalendarComponent, {
      disableClose: false,
      data: {
        id: clickInfo.event.id,
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(res)

    })

  }

  eventClick(event){
    console.log(event);
  }

}

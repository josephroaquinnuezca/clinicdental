import { ManageAppointmentComponent } from './../manage-appointment.component';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { isDataSource } from '@angular/cdk/collections';
import * as moment from 'moment'; // add this 1 of 4


@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {

  formCreate: FormGroup;

  dialogdata: any;

  getsingledata: any = [];

  name: any ;

  myDate = new Date(Date.now());

  categoryList: string[] = ['Tooth extraction', 'Fillings and Repair', 'Braces', 'Root canal', 'Teeth Whitening'];

  timeList: string[] = ['8:00am - 9:00am', '10:00am - 11:00am', '1:00pm - 2:00pm', '3:00pm - 4:00pm'];


  constructor(

    public clientService: ClientServiceService,
    public appointMentService: AppointServiceService,
    public dialogRef: MatDialogRef<ManageAppointmentComponent>,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    // this.dialogdata = results.id
   }

  ngOnInit(){

    // this.getSingleClient();

    this.formCreate = this.formgroup.group({
      // name: [this.name, Validators.required],
      appointmentdate: ['', Validators.required],
      appointmenttime: ['', Validators.required],
      category: ['', Validators.required],
    });

    const date =  moment(this.myDate).format('l')
    console.log("date", date)


  }

  //GET SINGLE DATA
  // getSingleClient(){
  //   console.log("dialog data")
  //   this.clientService.getSingleClient().subscribe(
  //       data => {
  //         this.getsingledata = data
  //         this.formCreate = this.formgroup.group({
  //           // name: [this.getsingledata.data.name, Validators.required ],
  //           appointmentdate: ['', Validators.required],
  //           appointmenttime: ['', Validators.required],
  //           category: ['', Validators.required],
  //         });
  //         console.log('data', this.getsingledata.data);
  //       })
  // }

  onSubmit(){

    if (this.formCreate.invalid) {

      this.formCreate.markAllAsTouched();
      this.toastr.error('Please Complete all required fields');
      return;


    }else{

      const modifytime = this.formCreate.get('appointmenttime').value;
      const modifycategory = this.formCreate.get('category').value;

      const removeTime = modifytime.toString().replace(/[#_]/g,'','');
      const removeCategory = modifycategory.toString().replace(/[#_]/g,'','');



      // console.log("hellomen", removeStr)

      const CreateAppointmentData = {
        // client_id: this.dialogdata,
        appointment_date: moment(this.formCreate.get('appointmentdate').value).format('l'),
        appointment_time: removeTime,
        appointment_category: removeCategory,


      }

      this.appointMentService.createAppointment(CreateAppointmentData).subscribe(data =>{
        console.log(data);
        this.toastr.success('Successfully Created Appointment');
      this.dialogRef.close();
      });

      console.log("sheytts", CreateAppointmentData);

    }


  }
}

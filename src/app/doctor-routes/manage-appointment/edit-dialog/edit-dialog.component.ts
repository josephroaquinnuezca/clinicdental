import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import * as moment from 'moment'; // add this 1 of 4
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
})
export class EditDialogComponent implements OnInit {

  disabledSubmitButton = false

  teethList: number[] = [];

  dialogdata: any;

  createform: FormGroup;
  myDate = new Date();

  categoryArray: any;


  ServiceValues:any = [];


  getSelectedData: any

  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  constructor(
    private datePipe: DatePipe,
    private router: Router,
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    private formgroup: FormBuilder,
    public ClientService: ClientServiceService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public results,
    ) {
      this.dialogdata = results
    }

  ngOnInit(): void {

    this.getAllServices();

    //start
    this.myDate.setDate(this.myDate.getDate());

    for (let i = 1; i <= 32; i++) {
      this.teethList.push(i);
    }

    this.createform = this.formgroup.group({
      appointmentdate: [moment(this.dialogdata.date).format(), Validators.required],
      appointmenttime: [this.dialogdata.time, Validators.required],
      category: [this.dialogdata.category, Validators.required],
      teeth_no:['']
    });
  }

  getAllServices() {
    this.ClientService.getAllService(this.paginate).subscribe((data) => {
      console.log(data);

      var result: any = data;

      console.log('Result', result.data.users);

      this.ServiceValues = result.data.users;
    });
  }

  timechange(event){

    const timeInputValue = event

    console.log("date change", event)

    this.appointMentService
       .appointmentTimeValidation(timeInputValue, this.dialogdata.id)
       .subscribe(
         (data) => {
           console.log(data);
           this.toastr.success('The appointment time that you selected is available');
           this.disabledSubmitButton= false
           // this.dialogRef.close();
         },(err) => {
           if (err instanceof HttpErrorResponse) {
             console.log(err.status);
             console.log('hello', err.error.description);
             this.toastr.error(err.error.description);
             this.disabledSubmitButton = true
            //  this.createform.controls.appointmentdate.setErrors({invalidDate:true})
           } else {
             console.log(err);
           }
         },
         () => {
           console.log('request completed');
         }
       );
  }
  dateChange(event){
    // Access the value of the input using event.target.value
    const dateinputvalue = moment(event.target.value).format('l');

    console.log("date change", dateinputvalue)

    this.appointMentService
       .appointentDateValidation(dateinputvalue, this.dialogdata.id)
       .subscribe(
         (data) => {
           console.log(data);
           this.toastr.success('The appointment that you selected is available');
           this.disabledSubmitButton= false
           // this.dialogRef.close();
         },(err) => {
           if (err instanceof HttpErrorResponse) {
             console.log(err.status);
             console.log('hello', err.error.description);
             this.toastr.error(err.error.description);
             this.disabledSubmitButton = true
             this.createform.controls.appointmentdate.setErrors({invalidDate:true})
           } else {
             console.log(err);
           }
         },
         () => {
           console.log('request completed');
         }
       );

 }


  onSubmit() {

      if (this.createform.invalid) {
        this.createform.markAllAsTouched();
        this.toastr.error('Please complete all required fields!!');
        return;
      } else {
        var values = JSON.parse(localStorage.getItem('dataSource'));


        console.log("date nt modify", this.createform.get('appointmentdate').value)

        const modifytime = moment(
          this.createform.get('appointmenttime').value
        ).format('LTS');
        const modifydate = this.createform.get('appointmentdate').value;

        console.log(modifytime);

        const CreateAppointment = {
          client_id: values.id,
          appointment_date: moment(
            this.createform.get('appointmentdate').value
          ).format('l'),
          appointment_time: this.createform.get('appointmenttime').value,
          appointment_category: JSON.stringify( this.createform.get('category').value),
          month: modifydate.toLocaleString('en-us', { month: 'long' }),
          // teeth_no:this.createform.get('teeth_no').value,
          // year: this.createform.get('category').value,
        };

        this.appointMentService
          .updateUserAppointment(CreateAppointment,  this.dialogdata.id)
          .subscribe(
            (data) => {
              console.log(data);
              this.toastr.success('Successfully Updated Appointment');
              this.dialogRef.close();
              // this.router.navigate(['/client-page/create-appointment']);
            },

            (err) => {
              if (err instanceof HttpErrorResponse) {
                console.log(err.status);
                console.log('hello', err.error.description);
                this.toastr.error(err.error.description);
              } else {
                console.log(err);
              }
            },
            () => {
              console.log('request completed');
            }
          );
      }
  } //onsubmit

}

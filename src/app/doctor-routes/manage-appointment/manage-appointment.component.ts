import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { ViewDialogComponent } from './view-dialog/view-dialog.component';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { Appointment } from 'src/app/_interface/appointment.model';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ToastrService } from 'ngx-toastr';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';

import * as moment from 'moment'; // add this 1 of 4


@Component({
  selector: 'app-manage-appointment',
  templateUrl: './manage-appointment.component.html',
  styleUrls: ['./manage-appointment.component.scss'],
})
export class ManageAppointmentComponent implements OnInit {

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  showErrorData: boolean = false;

  getAllData: any = [];



  dataSource: MatTableDataSource<Appointment>;
  displayedColumns: string[] = [
    'number',
    'date',
    'time',
    'category',
    'status',
    'controls',
  ];

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  myDate = new Date(Date.now());





  constructor(
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    private ClientService: ClientServiceService,
    public dialog: MatDialog,
    public _MatPaginatorIntl: MatPaginatorIntl //rename the item per page
  ) {}

  ngOnInit(): void {
    this._MatPaginatorIntl.itemsPerPageLabel = 'Display Number:';

      // NEW
      this.getAllClientAppointmentAdmin();

    const formdata = {};

    const date = moment(this.myDate).format('l');

    this.appointMentService.UpdateAlldateAppointment(date).subscribe(data => {
      console.log(data);
    });



  }


  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllClientAppointmentAdmin();


  }



  getAllClientAppointmentAdmin() {
    this.appointMentService.getAllAppointment(this.paginate).subscribe(async data => {

      console.log("manage appointment", data);

      var result: any = await data;

      this.getAllData = result.data.data;

      console.log("count", result.data.totalCount)

      this.paginate.totalCount = result.data.totalCount
      this.dataSource = new MatTableDataSource<any>(this.getAllData);
      this.dataSource.data = this.getAllData;
    });
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log("view search string", filterValue)

    this.paginator.pageIndex == 0
      ? this.getAllClientAppointmentAdmin()
      : this.paginator.firstPage();
  }



  addAppointment() {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClientAppointmentAdmin();
    });
  }

  Viewappointment() {
    const dialogRef = this.dialog.open(ViewDialogComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  deleteAppointment(id) {
    console.log(id);

    this.appointMentService.DeleteSingletAppointment(id).subscribe((data) => {
      this.toastr.success('Successfully Deleted');
      this.getAllClientAppointmentAdmin();

      console.log(data);
    });
  }

  editAppointment(id) {
    console.log('id', id);
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllClientAppointmentAdmin();
    });
  }
}




// StoredData: any = [];


  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;


// export interface PeriodicElement {
//   name: string;
//   weight: number;
//   symbol: string;
// }

// const ELEMENT_DATA: PeriodicElement[] = [
//   {name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {name: 'Boron', weight: 10.811, symbol: 'B'},
//   {name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];

  // dataSourceAll: MatTableDataSource<Appointment>;
  // displayedColumnsAll: string[] = [
  //   'number',
  //   'name',
  //   'date',
  //   'time',
  //   'category',
  //   'user_status',
  //   'conttrols',
  // ];


      // this.dataSource.data = this.getAllData.data;
      // this.dataSourceAll.paginator = this.paginator;
      // this.array = this.getAllData;
      // this.totalSize = this.array.length;
      // this.iterator();

      // this.dataSourceAll.data = this.getAllData;
  // public handlePage(e: any) {
  //   this.currentPage = e.pageIndex;
  //   this.pageSize = e.pageSize;
  //   this.iterator();
  // }

  // private iterator() {
  //   const end = (this.currentPage + 1) * this.pageSize;
  //   const start = this.currentPage * this.pageSize;
  //   const part = this.array.slice(start, end);
  //   this.dataSource = part;
  // }

  // getArray() {
  //   this.appointMentService.getAllAppointment().subscribe((data) => {
  //     this.StoredData = data;

  //     this.dataSource = new MatTableDataSource<Appointment>(
  //       this.StoredData.data
  //     );
  //     this.dataSource.paginator = this.paginator;
  //     this.array = this.StoredData.data;
  //     this.totalSize = this.array.length;
  //     this.iterator();
  //   });
  // }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this.dataSource.filter = filterValue;
  // }



 // getAllClientAppointmentAdmin(){
  //   this.appointMentService.getAllAppointment().subscribe((data) => {
  //     console.log(data);

  //     this.StoredData = data;

  //     this.dataSource = new MatTableDataSource<Appointment>(
  //       this.StoredData.data
  //     );
  //     // this.dataSource.paginator = this.paginator;
  //     // this.array = this.StoredData.data;
  //     // this.totalSize = this.array.length;
  //     // this.iterator();

  //     this.dataSource.data = this.StoredData.data;
  //   });
  // }

import { Component, OnInit } from '@angular/core';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import * as moment from 'moment'; // add this 1 of 4
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { UserGraphComponent } from './user-graph/user-graph.component';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-manage-reports',
  templateUrl: './manage-reports.component.html',
  styleUrls: ['./manage-reports.component.scss']
})
export class ManageReportsComponent implements OnInit {

  selectedYear: number;

  // /DECLARING VARIABLES HERE BEFORE YOU USE
  dataValues: any = [];

  dataValuesUser: any = [];

  //for spiiner
  isLoading = true;
  isLoadingAppointment = true;

  isShowUsers = false;
  isShowAppointment = false;

  dateValue: any;
  dateValueAppointment:any = []

  // form group
  peryearformappointment:  FormGroup;
  permonthformappointment:  FormGroup;
  peryearform: FormGroup;
  perweekform: FormGroup;
  permonthform: FormGroup;
  dateForm: FormGroup;
  dateFormAppointment: FormGroup

  //
  zero_one: boolean = false;
  zero_two: boolean = false;
  zero_three: boolean = false;
  zero_four: boolean = false;

  zero_five: boolean = false;
  zero_six: boolean = false;
  zero_seven: boolean = false;
  zero_eight: boolean = false;



  years: number[] = [];

  constructor(
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private ClientService: ClientServiceService,
    private appointmentservice: AppointServiceService,
  ) {
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 2000; year--) {
      this.years.push(year);
    }
   }

  ngOnInit() {

    // this.selectedYear = new Date().getFullYear();
    // for (let year = this.selectedYear; year >= 2020; year--) {
    //   this.years.push(year);
    // }

    this.dateForm = this.formgroup.group({
      datepick: [''],
    }
    );

    this.perweekform = this.formgroup.group({
      datepickstart: [''],
      datepickend: [''],
    }
    );

    this.permonthform = this.formgroup.group({
      monthreportuser: [''],
    }
    );

    this.permonthformappointment = this.formgroup.group({
      monthreportAppointment: [''],
    }
    );

    this.peryearformappointment = this.formgroup.group({
      yearreportappointment: [''],
    }
    );

    this.peryearform = this.formgroup.group({
      yearreportuser: [''],
    }
    );

    this.dateFormAppointment = this.formgroup.group({
      datepickAppointment: ['']
    }
    );
    // this.getAllcount();
  }

  onChangeEventUsers(event: any) {

    const datechangeValue = moment(event.target.value).format('l');

    console.log("date", datechangeValue)

    if(datechangeValue === ''){
      this.isLoading = true;

    }else{


      this.ClientService.getalllUsersBydate(datechangeValue).subscribe( data => {
        console.log(data);

        const result: any = data.data
        if(result === ''){
          this.isShowUsers = false;
        }else{
          this.dataValuesUser = result
          console.log("sheytts",this.dataValuesUser);
          this.isShowUsers = true;
        }
        // this.dataValues = result;
        // this.isLoading = false;

        // console.log(result);
      });



      this.ClientService.getalllCountBydate(datechangeValue).subscribe( data => {
      console.log(data);

      const result: any = data.data[0];
      if(result === ''){
        this.isShowUsers = false;
      }else{
        this.dateValue = datechangeValue
        this.dataValues = result;
        this.isShowUsers = true;
      }
      // this.dataValues = result;
      // this.isLoading = false;

      // console.log(result);
    });


    }





  }

  onChangeEventAppointment(event: any){

    const datechangeValueAppointment = moment(event.target.value).format('l');

    console.log("date", datechangeValueAppointment)

    if(datechangeValueAppointment === ''){
      this.isLoadingAppointment = true;

    }else{

      this.appointmentservice.GetAllReportUsersList(datechangeValueAppointment).subscribe( data => {
      console.log(data);

      const result: any = data.data;
      if(result === ''){
        this.isShowAppointment = false;
      }else{
        this.dateValueAppointment = result;
        this.isShowAppointment = true;
      }
      // this.dataValues = result;
      // this.isLoading = false;

      // console.log(result);
    });


    }

  }

  doSomething(event){
    console.log(event.value);
    console.log("event");

    if(event.value == 0){
      this.zero_one = true;
      // this.zero_two = false;
      this.zero_three = false;
      this.zero_four = false;
    }

            // if(event.value == 1){
            //   this.zero_one = false;
            //   this.zero_two = true;
            //   this.zero_three = false;
            //   this.zero_four = false;
            // }

    if(event.value == 2){
      this.zero_one = false;
      // this.zero_two = false;
      this.zero_three = true;
      this.zero_four = false;
    }

    if(event.value == 3){
      this.zero_one = false;
      // this.zero_two = false;
      this.zero_three = false;
      this.zero_four = true;
    }

  }


  onSubmit(){

    const startdate =  moment(this.perweekform.get('datepickstart').value).format('l');
    const enddate = moment( this.perweekform.get('datepickend').value).format('l');


    this.ClientService.gellAllperWeekUser(startdate, enddate).subscribe(data =>{

      const result: any = data.data

      if(result === ''){
        this.isShowUsers = false;
      }else{
        this.dataValuesUser = result
        this.isShowUsers = true;
      }

      // this.router.navigate(['/login']);
    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });

    console.log(startdate)
    console.log(enddate)

  }

  //monhtlyuserReport
  onsubmitMonthlyUser(){
    // const startdate =  moment(this.perweekform.get('datepickstart').value).format('l');
    console.log(this.permonthform.get('monthreportuser').value)

    const monthly = this.permonthform.get('monthreportuser').value ;
    console.log(monthly)

    this.ClientService.gellAllperMonhlyUser(monthly).subscribe(data =>{

      const result: any = data.data

      if(result === ''){
        this.isShowUsers = false;
      }else{
        this.dataValuesUser = result
        this.isShowUsers = true;
      }

      // this.router.navigate(['/login']);
    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }

   //monhtlyuserReport
   onsubmitMonthlyAppointment(){

    const monthly = this.permonthformappointment.get('monthreportAppointment').value ;
    console.log(monthly)

    if (this.permonthformappointment.invalid) {
      this.permonthformappointment.markAllAsTouched();
      this.snackBar.open('Please fill all the required fields', null, {
        duration: 3000,
      });
      return;


    } else {


    this.appointmentservice.gellAllperMonthAppointment(monthly).subscribe(data =>{

      const result: any = data.data

      console.log(result)

      if(result === ''){
        this.isShowAppointment = false;
      }else{
        this.dateValueAppointment = result
        this.isShowAppointment = true;
      }

      // this.router.navigate(['/login']);
    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });

    }



  }

  onsubmitYearUser(){
    const yearly = this.peryearform.get('yearreportuser').value ;
    console.log(yearly)


     this.ClientService.gellAllperYearUser(yearly).subscribe(data =>{

       const result: any = data.data

       if(result === ''){
         this.isShowUsers = false;
       }else{
         this.dataValuesUser = result
         this.isShowUsers = true;
       }

       // this.router.navigate(['/login']);
     }, err => {
       if (err instanceof HttpErrorResponse) {
         console.log(err.status)
         console.log("hello",err.error.description)
         this.toastr.error(err.error.description);
       }else{
         console.log(err)
       }

     }, () => {
       console.log('request completed')
     });



  }

  onsubmitYearAppointment(){
    const yearly = this.peryearformappointment.get('yearreportappointment').value ;
    console.log(yearly)


     this.appointmentservice.gellAllperYearAppointment(yearly).subscribe(data =>{

       const result: any = data.data

       if(result === ''){
        this.isShowAppointment = false;
      }else{
        this.dateValueAppointment = result
        this.isShowAppointment = true;
      }

      // this.router.navigate(['/login']);
    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });



  }

  appointmentselection(event){
      if(event.value == 0){
        this.zero_five = true;
        this.zero_six = false;
        this.zero_seven = false;
        this.zero_eight = false;
      }

      // if(event.value == 1){
      //   this.zero_five = false;
      //   this.zero_six = true;
      //   this.zero_seven = false;
      //   this.zero_eight = false;
      // }

      if(event.value == 2){
        this.zero_five = false;
        this.zero_six = false;
        this.zero_seven = true;
        this.zero_eight = false;
      }

      if(event.value == 3){
        this.zero_five = false;
        this.zero_six = false;
        this.zero_seven = false;
        this.zero_eight = true;
      }
  }

  // onChangeEventUsersStart(event){
  //   const datechangeValue = moment(event.target.value).format('l');

  //   console.log(datechangeValue)

  // }

  // onChangeEventUsersEnd(event){

  //   const datechangeValue = moment(event.target.value).format('l');

  //   console.log(datechangeValue)

  // }

  opengraphappointment(datavalues){

    console.log(datavalues)
    const dialogRef = this.dialog.open(UserGraphComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        datavalues,
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      // this.getAllClientAppointmentAdmin();
    });
  }






}

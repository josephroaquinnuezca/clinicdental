import { isDataSource } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'node_modules/chart.js';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';


@Component({
  selector: 'app-user-graph',
  templateUrl: './user-graph.component.html',
  styleUrls: ['./user-graph.component.scss']
})
export class UserGraphComponent implements OnInit {

  //declaration
  dialogData:any;

  dataValues: any = [];

  dataAppointment: any = [];

  dataChart: any = [];

  currentyear: any = [];

  myDate = new Date(Date.now());


  constructor(
    private ClientService: ClientServiceService,
    public appointMentService: AppointServiceService,
    public dialogRef: MatDialogRef<UserGraphComponent>,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
     this.dialogData = results
     Chart.register(
      ArcElement,
      LineElement,
      BarElement,
      PointElement,
      BarController,
      BubbleController,
      DoughnutController,
      LineController,
      PieController,
      PolarAreaController,
      RadarController,
      ScatterController,
      CategoryScale,
      LinearScale,
      LogarithmicScale,
      RadialLinearScale,
      TimeScale,
      TimeSeriesScale,
      Decimation,
      Filler,
      Legend,
      Title,
      Tooltip,
      SubTitle
    );
  }

  ngOnInit(): void {
    // this.chartFunction()
    this.getAllcount()

    console.log(this.dialogData.datavalues)
  }

  getAllcount() {
    this.ClientService.getalllCount().subscribe((data) => {
      console.log(data);

      const result: any = data.data[0];
      this.dataValues = result;

      this.dataChart = data;

      var ctx = document.getElementById('myChart'); //chart

      var myChart = new Chart('myChart', {
        type: 'bar',
        data: {
          labels: [
            'Confirmed',
            'Waiting',
            'Rejected',
            'Scheduled',

          ],
          datasets: [
            {
              label: '# of Votes',
              data: [
              this.dataValues.count_client_confirmed,
              this.dataValues.count_client_confirmed,
              this.dataValues.count_client_confirmed,
              this.dataValues.count_client_confirmed,
              ],
              backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderWidth: 1,
            },
          ],
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      });







      console.log(result);
    }); //end
  }



  chartFunction() {

    var ctx = document.getElementById('myChart');

    // this.ClientService.getalllCount().subscribe((data) => {

   const result: any = this.dialogData


    console.log('data one ', result);

    var myChart = new Chart('myChart', {
      type: 'bar',
      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        datasets: [
          {
            label: '# of Votes',
            data: [
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date,
              result.ceated_date
            ],
            backgroundColor: [
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(255, 99, 132, 0.2)',
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });

  // });

  }

}

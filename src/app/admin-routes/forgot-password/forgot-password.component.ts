import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  formlogin: FormGroup;

  constructor(
    private toastr: ToastrService,
    private formgroup: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.loginFormValidation()

  }
  loginFormValidation() {
    this.formlogin = this.formgroup.group({
      email: ['', Validators.required],
    });
  }

  submit(){
    if (this.formlogin.invalid) {
      //this is to show all the required fields
      this.formlogin.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    }else{
      this.toastr.success('Please check your email');
    }
  }


}

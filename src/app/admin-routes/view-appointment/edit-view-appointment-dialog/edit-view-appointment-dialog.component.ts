import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment'; // add this 1 of 4
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-edit-view-appointment-dialog',
  templateUrl: './edit-view-appointment-dialog.component.html',
  styleUrls: ['./edit-view-appointment-dialog.component.scss'],
})
export class EditViewAppointmentDialogComponent implements OnInit {
  createform: FormGroup;

  splitarrayTime: any;
  splitarrayCategory: any;

  storedAppointment: any = [];

  timeArray: any;
  categoryArray: any;

  subscription;

  testArray: any = [];

  dataSource: any = [];

  dialogdata: any;

  // myDate = Date.now();

  todayISOString: string = new Date().toISOString();
  myDate = new Date(Date.now());

  constructor(
    public dialogRef: MatDialogRef<EditViewAppointmentDialogComponent>,
    private toastr: ToastrService,
    private appointMentService: AppointServiceService,
    private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogdata = results;
  }

  ngOnInit() {
    this.createform = this.formgroup.group({
      appointment_date: ['', Validators.required],
      appointment_time: ['', Validators.required],
      appointment_category: ['', Validators.required],
    });

    this.getSingleAppointment();
  }

  onChangeEvent(event: any) {
    // const datechangeValue = moment(event.value).format('l');
    const datechangeValue = moment(event.target.value).format('l');

    let value = {
      datechangeValue: datechangeValue,
    };

    console.log(value);
    this.appointMentService.getSingleAppointment(value).subscribe(
      (data) => {
        this.storedAppointment = data;
        this.timeArray = this.storedAppointment.data.appointment_time;
        this.categoryArray = this.storedAppointment.data.appointment_category;

        const arrayStringTime = this.timeArray;
        const arrayStringCategory = this.categoryArray;

        this.splitarrayTime = arrayStringTime.split(',');
        this.splitarrayCategory = arrayStringCategory.split(',');

        // var keys = [];
        // for (var val of this.timeArray) {
        //   console.log(val); // prints values: 10, 20, 30, 40
        // }

        // let arraytest = Object.values(this.storedAppointment.data.appointment_time);
        //  this.testArray.push(this.timeArray)

        console.log('data Stored', this.storedAppointment.data);
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
          this.splitarrayCategory = [];
          this.splitarrayTime = [];
          this.createform.get('appointment_category').setValue('');
          this.createform.get('appointment_time').setValue('');
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please complete all required fields!!');
      return;
    } else {
      var values = JSON.parse(localStorage.getItem('dataSource'));

      const modifytime = this.createform.get('appointment_time').value;

      console.log(modifytime);

      const CreateAppointment = {
        client_id: values.id,
        appointment_date: moment(
          this.createform.get('appointment_date').value
        ).format('l'),
        appointment_time: this.createform.get('appointment_time').value,
        appointment_category: JSON.stringify(
          this.createform.get('appointment_category').value
        ),
      };

      this.appointMentService
        .updateUserAppointment(CreateAppointment, this.dialogdata.id)
        .subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('Successfully Updated Appointment');
            this.dialogRef.close();
            // this.createform.reset();
            // this.router.navigate(['/client-page/create-appointment']);
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );

      console.log('Form data', CreateAppointment);
      console.log('Form data', this.dialogdata.id);
    }
  }

  getSingleAppointment() {
    console.log('id', this.dialogdata.id);

    this.appointMentService
      .getSingleaAppointmentUser(this.dialogdata.id)
      .subscribe((data) => {
        console.log(data);

        var result: any = data;

        console.log('res', result.data);
        this.createform
          .get('appointment_date')
          .setValue(result.data.appointment_date);
        this.createform
          .get('appointment_time')
          .setValue(result.data.appointment_time);
        this.createform
          .get('appointment_category')
          .setValue(result.data.appointment_category);
      });
  }
}

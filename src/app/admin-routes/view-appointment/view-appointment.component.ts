import { CommentDialogComponent } from './comment-dialog/comment-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator, MatPaginatorIntl, PageEvent} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Appointment } from 'src/app/_interface/appointment.model';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { EditViewAppointmentDialogComponent } from './edit-view-appointment-dialog/edit-view-appointment-dialog.component';




@Component({
  selector: 'app-view-appointment',
  templateUrl: './view-appointment.component.html',
  styleUrls: ['./view-appointment.component.scss']
})



export class ViewAppointmentComponent implements OnInit {

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 5,
    page: 0,
    searchString: '',
  };



  StoredData: any = [];

  dataLocal:any = []

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  // @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['number', 'date', 'time', 'category', 'status', 'controls'];
  dataSource: MatTableDataSource<Appointment>;

  constructor(
    public appointMentService: AppointServiceService,
    public ClientService: ClientServiceService,
    public dialog: MatDialog,
    public _MatPaginatorIntl: MatPaginatorIntl, //rename the item per page
  ) { }

  ngOnInit(): void {

    const values = JSON.parse(localStorage.getItem("dataSource"));

    this.dataLocal = values

    this._MatPaginatorIntl.itemsPerPageLabel = 'Display Number:';

    this.getArray();


  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getArray();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;

    this.paginator.pageIndex == 0
      ? this.getArray()
      : this.paginator.firstPage();
  }

  getArray(){
    const values = JSON.parse(localStorage.getItem("dataSource"));

    const id = values.id;

    this.appointMentService.getAllClientAppointment(this.paginate,id).subscribe(data =>{
      console.log(data);

      var result: any = data;

      console.log("result",result);

      this.StoredData = result.data.users

      this.paginate.totalCount = result.data.totalCount
      this.dataSource = new MatTableDataSource<any>(this.StoredData);
      this.dataSource.data = this.StoredData;


      console.log("Appointment",  this.StoredData)

    });
  }



  openConfirmDialog(id) {

    console.log("id mo to", id)

    const dialogRef = this.dialog.open(EditViewAppointmentDialogComponent, {
      data: {
         id: id,
         },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.getArray();
    });
  }


  opencomment(){

    const dialogRef = this.dialog.open(CommentDialogComponent, {
      data: {

         },
    });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        // this.getArray();
      });
  }


}




// this.GetSingleClientApointment();

    // this.ClientService.getAllClient().subscribe(data =>{
    //   console.log(data);
    //   this.array = data;
    //   // this.dataSource = new MatTableDataSource<ClientInterface>(this.StoredData.data);

    //   // this.dataSource.data = this.StoredData.data;
    //   console.log("array", this.array)
    // })

    // public handlePage(e: any) {
    //   this.currentPage = e.pageIndex;
    //   this.pageSize = e.pageSize;
    //   this.iterator();
    // }



    // private iterator() {
    //   const end = (this.currentPage + 1) * this.pageSize;
    //   const start = this.currentPage * this.pageSize;
    //   const part = this.array.slice(start, end);
    //   this.dataSource = part;
    // }

    // this.array = this.StoredData.data;
    // this.totalSize = this.array.length;
    // this.iterator();

    // getArray() {
    //   const values = JSON.parse(localStorage.getItem("dataSource"));

    //   const id = values.id;

    //  this.appointMentService.getAllClientAppointment(id).subscribe(data =>{
    //       this.StoredData = data
    //       this.dataSource = new MatTableDataSource<Appointment>(this.StoredData.data);
    //       this.dataSource.paginator = this.paginator;

    //     });
    // }




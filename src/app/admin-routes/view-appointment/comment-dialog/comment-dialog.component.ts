import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment-dialog',
  templateUrl: './comment-dialog.component.html',
  styleUrls: ['./comment-dialog.component.scss']
})
export class CommentDialogComponent implements OnInit {

  name:any

  constructor() { }

  ngOnInit(): void {


    var values = JSON.parse(localStorage.getItem("dataSource"));

    this.name = values.name


  }

}

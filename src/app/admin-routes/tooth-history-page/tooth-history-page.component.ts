import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-tooth-history-page',
  templateUrl: './tooth-history-page.component.html',
  styleUrls: ['./tooth-history-page.component.scss']
})
export class ToothHistoryPageComponent implements OnInit {


  dataValues: any = [];
  dataLocal: any = [];
  storedData: any = [];

  myDate = new Date(Date.now());



  constructor(
    public appointMentService: AppointServiceService,
  ) { }

  ngOnInit() {


    const values = JSON.parse(localStorage.getItem("dataSource"));

    this.dataLocal = values

      //FUNCTION 1
     this.GetSingleClientApointment();

  }

  GetSingleClientApointment(){

    const values = JSON.parse(localStorage.getItem("dataSource"));


    const id = values.id;

    this.appointMentService.GetSingleUserTeeth(id).subscribe(data =>{

        console.log(data);
        const result: any = data;

        this.dataLocal = values
        this.dataValues = result.data
        // console.log("Appointment", this.dataValues.data)


    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        // this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });

  }


}

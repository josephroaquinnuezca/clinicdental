import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothHistoryPageComponent } from './tooth-history-page.component';

describe('ToothHistoryPageComponent', () => {
  let component: ToothHistoryPageComponent;
  let fixture: ComponentFixture<ToothHistoryPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToothHistoryPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothHistoryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

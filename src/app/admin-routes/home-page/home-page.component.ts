import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/app/_interface/appointment.model';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

import * as moment from 'moment'; // add this 1 of 4

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  dataValues: any = [];
  dataLocal: any = [];
  storedData: any = [];

  myDate = new Date(Date.now());

  constructor(
    public appointMentService: AppointServiceService,
  ) { }

  ngOnInit() {


    const values = JSON.parse(localStorage.getItem("dataSource"));

    this.dataLocal = values

      //FUNCTION 1
      this.GetSingleClientApointment();

  }

  GetSingleClientApointment(){

    const values = JSON.parse(localStorage.getItem("dataSource"));

    const date =  moment(this.myDate).format('l')

    const id = values.id;

    this.appointMentService.getSingleClientAppointment(id).subscribe(data =>{

        console.log(data);
        const result: any = data;

        this.dataLocal = values
        this.dataValues = result.data
        // console.log("Appointment", this.dataValues.data)


    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        // this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });

  }

}

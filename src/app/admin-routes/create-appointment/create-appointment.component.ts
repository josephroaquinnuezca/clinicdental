import { SetAppointmentWalkinDialogComponent } from './../../doctor-routes/manage-client-schedule/walkin-user-dialog/set-appointment-walkin-dialog/set-appointment-walkin-dialog.component';
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import * as moment from 'moment'; // add this 1 of 4
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import {
  MatPaginator,
  MatPaginatorIntl,
  PageEvent,
} from '@angular/material/paginator';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { MatDialog } from '@angular/material/dialog';
import { EditViewAppointmentDialogComponent } from '../view-appointment/edit-view-appointment-dialog/edit-view-appointment-dialog.component';
import { CommentDialogComponent } from '../view-appointment/comment-dialog/comment-dialog.component';
import { Appointment } from 'src/app/_interface/appointment.model';
import { CreateAppointmentDialogComponent } from '../create-appointment-dialog/create-appointment-dialog.component';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.scss'],
  providers: [DatePipe],
})
export class CreateAppointmentComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 5,
    page: 0,
    searchString: '',
  };

  StoredData: any = [];

  dataLocal: any = [];

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  // @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'number',
    'date',
    'time',
    'category',
    'status',
    'controls',
  ];
  dataSource: MatTableDataSource<Appointment>;


  show: boolean = false;
  hide: boolean = false;

  splitarrayTime: any;
  splitarrayCategory: any;

  storedAppointment: any = [];

  timeArray: any;
  categoryArray: any;

  subscription;

  testArray: any = [];



  // dataSource: any = []

  // myDate = Date.now();

  todayISOString: string = new Date().toISOString();

  myDate = new Date();

  constructor(
    private datePipe: DatePipe,
    private router: Router,
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    private formgroup: FormBuilder,
    public ClientService: ClientServiceService,
    public dialog: MatDialog,
    public _MatPaginatorIntl: MatPaginatorIntl //rename the item per page
  ) {}

  ngOnInit() {

    var values = JSON.parse(localStorage.getItem('dataSource'));
    this.dataSource = values;

    this.dataLocal = values

    console.log("datasource", this.dataSource)

    const time = new Date().getTime();
    console.log('time', time);

    const mydateme = moment(this.myDate).format('l');
    console.log('datemuko', mydateme);

    // this.getAllAppoint();

    this.getArray();


  }

  getKeys(obj) {
    return Object.keys(obj);
  }



  addAppointment() {
    const id = this.dataLocal.id;
    const dialogRef = this.dialog.open(SetAppointmentWalkinDialogComponent, {
      data: {
        id,
      }
      });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      // this.getAllClientAppointmentAdmin();
      this.getArray();
    });
  }

  // new
  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getArray();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;

    this.paginator.pageIndex == 0
      ? this.getArray()
      : this.paginator.firstPage();
  }

  getArray() {
    const values = JSON.parse(localStorage.getItem('dataSource'));

    const id = values.id;

    this.appointMentService
      .getAllClientAppointment(this.paginate, id)
      .subscribe((data) => {
        console.log(data);

        var result: any = data;

        console.log('result', result);

        this.StoredData = result.data.users;

        this.paginate.totalCount = result.data.totalCount;
        this.dataSource = new MatTableDataSource<any>(this.StoredData);
        this.dataSource.data = this.StoredData;

        console.log('Appointment', this.StoredData);
      });
  }

  openConfirmDialog(id) {
    console.log('id mo to', id);

    const dialogRef = this.dialog.open(EditViewAppointmentDialogComponent, {
      data: {
        id: id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getArray();
    });
  }

  opencomment() {
    const dialogRef = this.dialog.open(CommentDialogComponent, {
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      // this.getArray();
    });
  }
}

// onChangeEvent(event: any) {
//   // const datechangeValue = moment(event.value).format('l');
//   const datechangeValue = moment(event.target.value).format('l');

//   let value = {
//     datechangeValue: datechangeValue,
//   };

//   console.log(value);
//   this.appointMentService.getSingleAppointment(value).subscribe(
//     (data) => {
//       this.storedAppointment = data;
//       this.timeArray = this.storedAppointment.data.appointment_time;
//       this.categoryArray = this.storedAppointment.data.appointment_category;

//       const arrayStringTime = this.timeArray;
//       const arrayStringCategory = this.categoryArray;

//       this.splitarrayTime = arrayStringTime.split(',');
//       this.splitarrayCategory = arrayStringCategory.split(',');

//       console.log('data Stored', this.storedAppointment.data);
//     },
//     (err) => {
//       if (err instanceof HttpErrorResponse) {
//         console.log(err.status);
//         console.log('hello', err.error.description);
//         this.toastr.error(err.error.description);
//         this.splitarrayCategory = [];
//         this.splitarrayTime = [];
//         this.createform.get('category').setValue('');
//         this.createform.get('appointmenttime').setValue('');
//       } else {
//         console.log(err);
//       }
//     },
//     () => {
//       console.log('request completed');
//     }
//   );
// }

// this.myHolidayFilter();

// this.myHolidayFilter()

// this.backbtn();

// const foreachval = this.timeArray
// foreachval.forEach(this.foreachfunction)

// const time = moment(this.myDate).format('l'); (new Date()).getTime();

// console.log("time", time)

// dateClass = (d: Date) => {
//   const date = d.getDay();
//   // Highlight saturday and sunday.
//   return date === 0 || date === 6 ? 'highlight-dates' : undefined;
// };

// var keys = [];
// for (var val of this.timeArray) {
//   console.log(val); // prints values: 10, 20, 30, 40
// }

// let arraytest = Object.values(this.storedAppointment.data.appointment_time);
//  this.testArray.push(this.timeArray)

// myHolidayDates = [
//   new Date('9/4/2021')
// ];

//  time = new Date().getTime();

//   myHolidayFilter = (d: Date): boolean => {
//     // const time = d.getTime();
//     return !this.myHolidayDates
// }

// ngOnDestroy() {
//   this.subscription.unsubscribe()
// }

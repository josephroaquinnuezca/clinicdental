import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import * as moment from 'moment'; // add this 1 of 4
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
})
export class CreateAccountComponent implements OnInit {
  mobNumberPattern = '^((\\+91-?)|0)?[0-9]{10}$';

  // please import the FormGroup Module first
  hide = true;
  createform: FormGroup;
  submitted = false;

  minimumdate: any;

  myDate = new Date(Date.now());

  // minDate: moment.Moment;

  constructor(
    private router: Router,
    public clientService: ClientServiceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder
  ) {}

  ngOnInit(): void {
    this.minimumdate = new Date();

    this.CreateFormValidation();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  CreateFormValidation() {
    this.createform = this.formgroup.group(
      {
        username: ['', Validators.required],
        fname: ['', Validators.required],
        mname: [''],
        lname: ['', Validators.required],
        email_add: ['', [Validators.required, Validators.email]],
        contact_no: [
          '',
          [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            Validators.minLength(11),
            Validators.maxLength(11),
          ],
        ],
        address: ['', Validators.required],
        password: ['', Validators.required, Validators.minLength(6)],
        re_enterpass: ['', Validators.required, Validators.minLength(6)],
      },
      { validator: this.checkIfMatchingPasswords('password', 're_enterpass') }
    );
  }

  checkIfMatchingPasswords(
    passwordKey: string,
    passwordConfirmationKey: string
  ) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  get f() {
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;

    } else {
      const date = moment(this.myDate).format('l');

      const dataClient = {
        fname: this.createform.get('fname').value,
        mname: this.createform.get('mname').value,
        lname: this.createform.get('lname').value,
        dob: '',
        gender: '',
        username: this.createform.get('username').value,
        password: this.createform.get('password').value,
        contact_no: this.createform.get('contact_no').value,
        age: '',
        email: this.createform.get('email_add').value,
        address: this.createform.get('address').value,
        date_created: date,
      };

      this.clientService.createClient(dataClient).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.router.navigate(['/login']);
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );

      console.log('USER ACCOUNT', dataClient);
    }
  }
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
// import { ErrorDialogComponent } from 'src/app/_reusesable-dialogs/error-dialog/error-dialog.component';/
import { ErrorReusesableDialogsService } from 'src/app/_reusesable-dialogs/service-dialog/error-reusesable-dialogs.service';
import { TokenStorageService } from 'src/app/_shared-core/token-storage.service';
import { LoginServiceService } from 'src/app/_shared/shared/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formlogin: FormGroup;
  submitted = false;

  dataError: any;

  localData: any = [];

  constructor(
    private tokenservice: TokenStorageService,
    private router: Router,
    public loginService: LoginServiceService,
    private formgroup: FormBuilder,
    private toastr: ToastrService
  ) {
    // this.loginForm = this.loginFormValidation();
  }

  ngOnInit() {
    this.loginFormValidation();
  }

  get f() {
    return this.formlogin.controls;
  }

  loginFormValidation() {
    this.formlogin = this.formgroup.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.formlogin.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onSubmit() {
    if (this.formlogin.invalid) {
      //this is to show all the required fields
      this.formlogin.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {
      const username = this.formlogin.get('username').value;
      const password = this.formlogin.get('password').value;

      console.log('LOGIN VALUE', username, password);

      this.loginService.login(username, password).subscribe(
        (data) => {
          console.log('data login', data);

          // this.localData = data

          const result: any = data;

          console.log(result.data.token);

          this.tokenservice.saveToken(result.data.token);

          const newToken = this.tokenservice.decodeToken(result.data.token);

          localStorage.setItem(
            'dataSource',
            JSON.stringify(newToken.auth_data)
          );

          console.log('new token', newToken);

          if (
            newToken.auth_data.user_role == 1 ||
            newToken.auth_data.user_role == 2 ||
            newToken.auth_data.user_role == 3 ||
            newToken.auth_data.user_role == 4
          ) {
            this.toastr.success('Successfully Login');
            this.router.navigate(['/admin-page/school-dashboard']);
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

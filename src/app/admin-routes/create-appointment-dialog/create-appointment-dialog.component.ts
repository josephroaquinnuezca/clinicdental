import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-create-appointment-dialog',
  templateUrl: './create-appointment-dialog.component.html',
  styleUrls: ['./create-appointment-dialog.component.scss'],
})
export class CreateAppointmentDialogComponent implements OnInit {
  createform: FormGroup;
  myDate = new Date();

  categoryArray: any;


  ServiceValues:any = [];


  getSelectedData: any

  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  constructor(
    private datePipe: DatePipe,
    private router: Router,
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    private formgroup: FormBuilder,
    public ClientService: ClientServiceService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateAppointmentDialogComponent>,
  ) {}

  ngOnInit(): void {
    //start
    this.myDate.setDate(this.myDate.getDate() + 1);

    this.getAllServices();

    this.createform = this.formgroup.group({
      appointmentdate: ['', Validators.required],
      appointmenttime: ['', Validators.required],
      category: ['', Validators.required],
    });
  }


  getAllServices() {
    this.ClientService
      .getAllService(this.paginate)
      .subscribe((data) => {
        console.log(data);

        var result: any = data;

        console.log("Result", result.data.users)

        this.ServiceValues = result.data.users

        // this.getAllData = result.data.users;
        // this.paginate.totalCount = result.data.totalCount;
        // this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
        // this.dataSourceAll.data = this.getAllData;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  onSubmit() {
    // const arrayStrings =  this.createform.get('category').value

    // const arrayObjectCategory =   arrayStrings.map(String);



    // console.log(arrayObjectCategory);

      if (this.createform.invalid) {
        this.createform.markAllAsTouched();
        this.toastr.error('Please complete all required fields!!');
        return;
      } else {
        var values = JSON.parse(localStorage.getItem('dataSource'));

        const modifytime = moment(
          this.createform.get('appointmenttime').value
        ).format('LTS');
        const modifydate = this.createform.get('appointmentdate').value;

        console.log(modifytime);

        const CreateAppointment = {
          client_id: values.id,
          appointment_date: moment(
            this.createform.get('appointmentdate').value
          ).format('l'),
          appointment_time: this.createform.get('appointmenttime').value,
          appointment_category: JSON.stringify( this.createform.get('category').value),
          month: modifydate.toLocaleString('en-us', { month: 'long' }),
          // year: this.createform.get('category').value,
        };

        this.appointMentService
          .createClientAppointment(CreateAppointment)
          .subscribe(
            (data) => {
              console.log(data);
              this.toastr.success('Successfully Created Appointment');
              this.dialogRef.close();
              // this.router.navigate(['/client-page/create-appointment']);
            },

            (err) => {
              if (err instanceof HttpErrorResponse) {
                console.log(err.status);
                console.log('hello', err.error.description);
                this.toastr.error(err.error.description);
              } else {
                console.log(err);
              }
            },
            () => {
              console.log('request completed');
            }
          );

        console.log('sheytts', CreateAppointment);
      }
  } //onsubmit
}


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
// import { MapComponent } from '../map/map.component';
import { ToastrService } from 'ngx-toastr';


import * as moment from 'moment'; // add this 1 of 4
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";

  signupform: FormGroup;
  hide = true;
  submitted = false;

  DataValues:any;

  minDate: moment.Moment;

  constructor(
    public dialogRef: MatDialogRef<ProfileComponent>,
    public dialog: MatDialog,
    private formgroup: FormBuilder,
    public clientService: ClientServiceService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {

    this.getSingleUsersData();

     const currentYear = moment().year();
    this.minDate = moment([currentYear - 1, 0, 1]);


    // const phoneValidation = "^(\+)(\d){12}$)|(^\d{11}$";

    var values = JSON.parse(localStorage.getItem("dataSource"));

    this.signupform = this.formgroup.group({
      fname: [values.fname, Validators.required],
      mname: [values.mname, Validators.required],
      lname: [values.lname, Validators.required],
      dob: [values.dob, Validators.required],
      gender: [values.gender, Validators.required],
      username: [values.username, Validators.required], 
      password: [''],
      retypepassword: [''],
      mobile: [values.contact_no, [Validators.required]],
      email: [values.email, [Validators.required, Validators.email]],
      address:[values.address, Validators.required],
    },{validator: this.checkIfMatchingPasswords('password', 'retypepassword')}
    );


  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }



   //check the password match
   checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
          passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
          return passwordConfirmationInput.setErrors(null);
      }
    }
  }

   get f() { return this.signupform.controls; }

   onSubmit(){

    if (this.signupform.invalid) {

      this.signupform.markAllAsTouched();

      this.toastr.error('Please Complete all required fields!');

      return;


    }else{
      var values = JSON.parse(localStorage.getItem("dataSource"));

      const age =  moment().diff(this.signupform.get('dob').value, 'years') //age

      const create = {

        fname: this.signupform.get('fname').value,
        mname: this.signupform.get('mname').value,
        lname: this.signupform.get('lname').value,
        dob: this.signupform.get('dob').value,
        gender: this.signupform.get('gender').value,
        username: this.signupform.get('username').value,
        password: this.signupform.get('password').value,
        contact_no: this.signupform.get('mobile').value,
        age: age,
        email: this.signupform.get('email').value,
        address: this.signupform.get('address').value,

        }

        this.clientService.updateClient(create, values.id).subscribe(data =>{
          console.log(data);
          this.dialogRef.close();
          window.sessionStorage.clear();
          window.location.reload();

        }, err => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status)
            console.log("hello",err.error.description)
            this.toastr.error(err.error.description);
          }else{
            console.log(err)
          }

        }, () => {
          console.log('request completed')
        });

        console.log("CREATE CLIENT", create);

    }

   }


    getSingleUsersData(){

      var values = JSON.parse(localStorage.getItem("dataSource"));
      this.DataValues = values;
      console.log("data values", values)

      this.signupform = this.formgroup.group({
        fname: [values.fname, Validators.required],
        mname: [values.mname, Validators.required],
        lname: [values.lname, Validators.required],
        dob: [values.dob, Validators.required],
        gender: [values.gender, Validators.required],
        username: [values.username, Validators.required],
        password: ['', Validators.required],
        retypepassword: ['', Validators.required],
        mobile: [values.mobile, [Validators.required]],
        email: [values.email, [Validators.required, Validators.email]],
        address:[values.address, Validators.required],
      },{validator: this.checkIfMatchingPasswords('password', 'retypepassword')}
      );


    }

  //  openMap(){
  //   const dialogRef = this.dialog.open(MapComponent, {
  //     maxWidth: '100vw',
  //     maxHeight: '100vh',
  //     height: '100%',
  //     width: '100%'
  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   // dialogRef.afterClosed().subscribe(result => {
  //   //   console.log('The dialog was closed');
  //   //   this.animal = result;
  //   // });
  //  }

}


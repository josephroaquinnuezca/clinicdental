import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-add-note-teeth-dialog',
  templateUrl: './add-note-teeth-dialog.component.html',
  styleUrls: ['./add-note-teeth-dialog.component.scss']
})
export class AddNoteTeethDialogComponent implements OnInit {

  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";

  dialogdata: any;

  // please import the FormGroup Module first
  hide = true;
  createform: FormGroup;
  submitted = false;

  minimumdate:any;

  myDate = new Date(Date.now());

  // minDate: moment.Moment;

  constructor(
    @Inject(MAT_DIALOG_DATA) public results,
    private router: Router,
    public clientService: ClientServiceService,
    public appointmentSevice: AppointServiceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    public dialogRef: MatDialogRef<AddNoteTeethDialogComponent>,
  ) {
    this.dialogdata = results;
   }

  ngOnInit(): void {



    this.minimumdate = new Date();

    this.CreateFormValidation();
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }



  CreateFormValidation(){
    this.createform = this.formgroup.group({
      message: ['', Validators.required],
      teeth_no: [this.dialogdata.id, Validators.required],
    }
    );
  }


  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
          passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
          return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  get f(){
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    return invalid;
  }


  onSubmit(){

    if (this.createform.invalid) {

      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;

    }else{

      // const date = moment(this.myDate).format('l');

      const dataClient = {

        client_id:this.dialogdata.client_id,
        note:this.createform.get('message').value,
        teeth_no:this.createform.get('teeth_no').value,


      }

      console.log("Data Client", dataClient)

      this.appointmentSevice.addNoteInTeeth(dataClient).subscribe(data =>{
        console.log(data);
        this.toastr.success('Successfully Registered');
        this.dialogRef.close();
        // this.router.navigate(['/login']);
      }, err => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status)
          console.log("hello",err.error.description)
          this.toastr.error(err.error.description);
        }else{
          console.log(err)
        }

      }, () => {
        console.log('request completed')
      });


    }

   }

}

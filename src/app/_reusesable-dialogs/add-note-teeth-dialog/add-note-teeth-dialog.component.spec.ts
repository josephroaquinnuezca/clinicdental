import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNoteTeethDialogComponent } from './add-note-teeth-dialog.component';

describe('AddNoteTeethDialogComponent', () => {
  let component: AddNoteTeethDialogComponent;
  let fixture: ComponentFixture<AddNoteTeethDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNoteTeethDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNoteTeethDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

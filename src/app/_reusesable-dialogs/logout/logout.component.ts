import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LoginServiceService } from 'src/app/_shared/shared/login-service.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<LogoutComponent>,
    public loginService: LoginServiceService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  logout() {
    this.loginService.logout().subscribe((data) => {
      console.log('DAta', data);
      this.dialogRef.close();
      window.sessionStorage.clear();
      window.location.reload();
      this.router.navigate(['/login']);
    });
  }
}

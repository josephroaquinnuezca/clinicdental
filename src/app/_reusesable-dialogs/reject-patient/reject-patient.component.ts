import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-reject-patient',
  templateUrl: './reject-patient.component.html',
  styleUrls: ['./reject-patient.component.scss'],
})
export class RejectPatientComponent implements OnInit {
  dialogdata: any;

  constructor(
    public dialogRef: MatDialogRef<RejectPatientComponent>,
    // private datePipe: DatePipe,
    // private router: Router,
    private toastr: ToastrService,
    public clientService: ClientServiceService,
    // private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogdata = results;
  }

  ngOnInit(): void {}

  rejectDialog() {
    this.clientService.RejectClient(this.dialogdata.id).subscribe((data) => {
      console.log(data);
      var result: any = data;
      this.toastr.success('Successfully reject the user');
      this.dialogRef.close();
    });
  }
}

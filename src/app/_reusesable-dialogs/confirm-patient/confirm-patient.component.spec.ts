import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmPatientComponent } from './confirm-patient.component';

describe('ConfirmPatientComponent', () => {
  let component: ConfirmPatientComponent;
  let fixture: ComponentFixture<ConfirmPatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmPatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

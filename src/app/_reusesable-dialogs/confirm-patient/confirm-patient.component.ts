import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-confirm-patient',
  templateUrl: './confirm-patient.component.html',
  styleUrls: ['./confirm-patient.component.scss']
})
export class ConfirmPatientComponent implements OnInit {

  dialogdata: any;

  constructor(

    // private datePipe: DatePipe,
    // private router: Router,
    private toastr: ToastrService,
    public clientService: ClientServiceService,
    // private formgroup: FormBuilder,
    public dialogRef: MatDialogRef<ConfirmPatientComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogdata = results;
  }

  ngOnInit(): void {}

  confirmDialog() {
    this.clientService.ConfirmedClient(this.dialogdata.id).subscribe((data) => {
      console.log(data);
      var result: any = data;
      this.toastr.success('Successfully confirmed the user');
      this.dialogRef.close();
    });
  }

}

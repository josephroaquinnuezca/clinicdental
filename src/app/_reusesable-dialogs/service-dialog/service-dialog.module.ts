import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialsModule } from 'src/app/shared/materials.module';

@NgModule({
  declarations: [],
  exports: [],
  imports: [MaterialsModule],
  entryComponents: [],
})
export class ServiceDialogModule {}

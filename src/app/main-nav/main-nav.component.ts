import { ProfileComponent } from './../_reusesable-dialogs/profile/profile.component';
import { LogoutComponent } from './../_reusesable-dialogs/logout/logout.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {AfterViewInit, ElementRef, ViewChild} from '@angular/core';


@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent implements OnInit {

  @ViewChild('mySidenav',{static: true}) mySidenav: ElementRef;
  @ViewChild('useThisTemplateVar',{static: true}) changeIcon: ElementRef;

  values: any

  constructor(
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.values = JSON.parse(localStorage.getItem('dataSource'));
  }




  openNav(){

    // alert("hello")
    this.mySidenav.nativeElement.classList.toggle('active');
    this.changeIcon.nativeElement.classList.toggle('fa-xmark');




    // this.myDiv.nativeElement.classList.contains('active');

  }

  openDialogLogout(): void {
    const dialogRef = this.dialog.open(LogoutComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }

  openProfileDialog(){
    const dialogRef = this.dialog.open(ProfileComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
    });
  }
}

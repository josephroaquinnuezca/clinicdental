import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ServicesAddDialogComponent } from 'src/app/services/module/dialog/services-add-dialog/services-add-dialog.component';
import { ClientInterface } from 'src/app/_interface/client-interface.model';
import { ClientServiceService } from 'src/app/_shared/shared/client-service.service';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.scss']
})
export class TransactionHistoryComponent implements OnInit {

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  // public array: any;
  // public pageSize = 5;
  // public currentPage = 0;
  // public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  dataSourceAll: MatTableDataSource<ClientInterface>;
  getAllData: any = [];

  displayedColumnsAll: string[] = [
    'number',
    'name',
    'service_name',
    'price',
    'total',
    'date',
    'conttrols',
  ];

  constructor(
    public dialog: MatDialog,
    public ClientServiceService: ClientServiceService,
  ) {}

  ngOnInit() {
    this.getAllTransaction();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllTransaction();
  }

  getAllTransaction() {
    this.ClientServiceService
      .getAllTransaction(this.paginate)
      .subscribe((data) => {
        console.log(data);

        console.log("fuk me ",data)

        var result: any = data;

        this.getAllData = result.data.users;
        this.paginate.totalCount = result.data.totalCount;
        this.dataSourceAll = new MatTableDataSource<any>(this.getAllData);
        this.dataSourceAll.data = this.getAllData;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllTransaction()
      : this.paginator.firstPage();
  }

  addServices() {
    const dialogRef = this.dialog.open(ServicesAddDialogComponent, {

    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.getAllTransaction()
    });
  }


}

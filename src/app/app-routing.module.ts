import { ManageReportsComponent } from './doctor-routes/manage-reports/manage-reports.component';
import { ServiceLandingPageComponent } from './static-routes/service-landing-page/service-landing-page.component';
import { HomeLandingPageComponent } from './static-routes/home-landing-page/home-landing-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { HomePageComponent } from './admin-routes/home-page/home-page.component';
import { ManagePatientsComponent } from './doctor-routes/manage-patients/manage-patients.component';
import { ManageAppointmentComponent } from './doctor-routes/manage-appointment/manage-appointment.component';
import { StaffComponent } from './doctor-routes/staff/staff.component';
import { DoctorDashboardComponent } from './doctor-routes/doctor-dashboard/doctor-dashboard.component';
import { DocktorNavComponent } from './navigation/docktor-nav/docktor-nav.component';
import { CreateAccountComponent } from './admin-routes/create-account/create-account.component';
import { ForgotPasswordComponent } from './admin-routes/forgot-password/forgot-password.component';
// import { DashboardAppointmentComponent } from './admin-routes/dashboard-appointment/dashboard-appointment.component';
import { ViewAppointmentComponent } from './admin-routes/view-appointment/view-appointment.component';
import { CreateAppointmentComponent } from './admin-routes/create-appointment/create-appointment.component';
import { LoginComponent } from './admin-routes/login/login.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './_shared-core/auth-guard.guard';
import { CalendarScheduleComponent } from './doctor-routes/calendar-schedule/calendar-schedule.component';
import { ManageClientScheduleComponent } from './doctor-routes/manage-client-schedule/manage-client-schedule.component';
import { CalendarDialogComponent } from './_reusesable-dialogs/calendar-dialog/calendar-dialog.component';
import { AdminLoginComponent } from './doctor-routes/admin-login/admin-login.component';
import { HeaderToolbarComponent } from './_reusesable-dialogs/header-toolbar/header-toolbar.component';
import { ServicesListComponent } from './services/module/services-list/services-list.component';
import { TransactionHistoryComponent } from './transaction/module/transaction-history/transaction-history.component';
import { ToothHistoryPageComponent } from './admin-routes/tooth-history-page/tooth-history-page.component';
import { NavigationComponent } from './school-property/navigation/navigation.component';
import { BuildingComponent } from './school-property/building/building.component';
import { CustodianComponent } from './school-property/custodian/custodian.component';
import { RoomsComponent } from './school-property/rooms/rooms.component';
import { UsersAccountComponent } from './school-property/users-account/users-account.component';
import { SchoolHistoryComponent } from './school-property/school-history/school-history.component';
import { SchoolReportsComponent } from './school-property/school-reports/school-reports.component';
import { SchoolDashboardComponent } from './school-property/school-dashboard/school-dashboard.component';
import { LaboratoryComponent } from './school-property/laboratory/laboratory.component';
import { BorrowItemListComponent } from './school-property/borrow-item-list/borrow-item-list.component';
import { StocksMgntComponent } from './school-property/custodian/stocks-mgnt/stocks-mgnt.component';
import { StatusManagementListComponent } from './school-property/status-management-list/status-management-list.component';
import { HomepagePropertyComponent } from './school-property/homepage-property/homepage-property.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: LandingPageComponent,
  //   children: [
  //     { path: 'home-landing-page', component: HomeLandingPageComponent },
  //     { path: 'service-landing-page', component: ServiceLandingPageComponent },

  //     // { path: '', loadChildren: () => import('./admin-routes/admin.module').then(m => m.AdminModule) },
  //   ],
  // },

  {
    path: 'client-page',
    component: MainNavComponent,
    canActivate: [AuthGuardGuard],
    children: [
      { path: 'create-appointment', component: CreateAppointmentComponent },
      { path: 'view-appointment', component: ViewAppointmentComponent },
      { path: 'client-calendar', component: CalendarDialogComponent },
      { path: 'home', component: HomePageComponent },
      { path: 'tooth_history', component: ToothHistoryPageComponent },

      // { path: '', loadChildren: () => import('./admin-routes/admin.module').then(m => m.AdminModule) },
    ],
  },

  {
    path: 'doctor-page',
    component: DocktorNavComponent,
    children: [
      { path: 'doctor-dashboard', component: DoctorDashboardComponent },
      { path: 'doctor-appointments', component: ManageClientScheduleComponent },
      { path: 'doctor-patients', component: ManagePatientsComponent },
      { path: 'doctor-reports', component: ManageReportsComponent },
      { path: 'staff-page', component: StaffComponent },
      { path: 'calendar-schedule', component: CalendarScheduleComponent },
      { path: 'services-list', component: ServicesListComponent },
      { path: 'transaction-history', component: TransactionHistoryComponent },
    ],
  },

  {
    path: 'admin',
    component: HeaderToolbarComponent,
    children: [{ path: 'doctor-login', component: AdminLoginComponent }],
  },

  { path: '', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'create-account', component: CreateAccountComponent },

  {
    path: 'admin-page',
    component: NavigationComponent,
    canActivate: [AuthGuardGuard],
    children: [
      { path: 'custodian-inventory', component: CustodianComponent },
      { path: 'building-inventory', component: BuildingComponent },
      { path: 'rooms-mngt', component: RoomsComponent },
      { path: 'comlab-mngt', component: LaboratoryComponent },
      { path: 'users-mngt', component: UsersAccountComponent },
      { path: 'users-history', component: SchoolHistoryComponent },
      { path: 'users-report', component: SchoolReportsComponent },
      { path: 'school-dashboard', component: SchoolDashboardComponent },
      { path: 'borrowed-items', component: BorrowItemListComponent },
      { path: 'stocks-mgnt', component: StocksMgntComponent },
      { path: 'status-mgnt', component: StatusManagementListComponent },
      { path: 'home-page-mgnt', component: HomepagePropertyComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}

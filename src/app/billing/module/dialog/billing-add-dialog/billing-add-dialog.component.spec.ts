import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingAddDialogComponent } from './billing-add-dialog.component';

describe('BillingAddDialogComponent', () => {
  let component: BillingAddDialogComponent;
  let fixture: ComponentFixture<BillingAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillingAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

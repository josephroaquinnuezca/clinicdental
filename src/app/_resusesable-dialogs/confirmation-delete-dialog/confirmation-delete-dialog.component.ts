import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';

@Component({
  selector: 'app-confirmation-delete-dialog',
  templateUrl: './confirmation-delete-dialog.component.html',
  styleUrls: ['./confirmation-delete-dialog.component.scss'],
})
export class ConfirmationDeleteDialogComponent implements OnInit {


  createform: FormGroup;


  dialogdata: any;


  constructor(
    public dialogRef: MatDialogRef<ConfirmationDeleteDialogComponent>,
    // private datePipe: DatePipe,
    // private router: Router,
    private toastr: ToastrService,
    public appointMentService: AppointServiceService,
    private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogdata = results;
  }

  ngOnInit(): void {
    this.CreateFormValidation();
  }

  CreateFormValidation() {
    this.createform = this.formgroup.group(
      {
        comment: [''],

      }
    );
  }

  deleteAppointment() {



    this.appointMentService
      .rejectAppointmentClient(this.dialogdata.id, this.dialogdata.contactno, this.createform.get('comment').value,)
      .subscribe((data) => {
        console.log(data);
        const result: any = data;
        this.toastr.success('Successfully Cancelled the appointment created');
        this.dialogRef.close()
      });
  }
}

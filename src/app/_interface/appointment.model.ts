export interface Appointment {
  id: number;
  client_id: number;
  appointment_date: string;
  appointment_time: string;
  appointment_category: string;
}

export interface ClientInterface {
  id: number;
  name: string;
  contact_no: string;
  email: string;
  emailaddress: string;
}

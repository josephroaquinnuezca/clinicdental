import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from '../shared/school-property.service';
import { BorrowItemsComponent } from '../laboratory/borrow-items/borrow-items.component';
import { ReturnItemListComponent } from './return-item-list/return-item-list.component';

@Component({
  selector: 'app-borrow-item-list',
  templateUrl: './borrow-item-list.component.html',
  styleUrls: ['./borrow-item-list.component.scss'],
})
export class BorrowItemListComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'item_name',
    'borrower',
    'item_quantity',
    'item_brand',
    'item_category',
    'module',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllBorrowItems();
  }

  createRoom() {
    const dialogRef = this.dialog.open(BorrowItemsComponent, {
      // data: {
      //   id,
      // }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBorrowItems();
      console.log('The dialog was closed', result);
    });
  }

  onclickReturn(element) {
    const dialogRef = this.dialog.open(ReturnItemListComponent, {
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBorrowItems();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    // const dialogRef = this.dialog.open(DeleteCustodianComponent, {
    //   data: {
    //     id,
    //   },
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log('The dialog was closed', result);
    // });
  }

  onclickStockIn(id) {
    // const dialogRef = this.dialog.open(StockInComponent, {
    //   data: {
    //     id,
    //   },
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log('The dialog was closed', result);
    // });
  }

  onclickStockOut(id) {
    // const dialogRef = this.dialog.open(StockOutComponent, {
    //   data: {
    //     id,
    //   },
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log('The dialog was closed', result);
    // });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllBorrowItems();
  }

  getAllBorrowItems() {
    this.SchoolPropertyService.getAllBorrowItems(this.paginate).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllBorrowItems()
      : this.paginator.firstPage();
  }
}

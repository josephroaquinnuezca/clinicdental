import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BorrowItemListComponent } from '../borrow-item-list.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-return-item-list',
  templateUrl: './return-item-list.component.html',
  styleUrls: ['./return-item-list.component.scss'],
})
export class ReturnItemListComponent implements OnInit {
  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  itemArr: any = [];

  createform: FormGroup;

  elementData: any = [];
  selected: any;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<BorrowItemListComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.elementData = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: [this.elementData.element?.item_name, Validators.required],
      borrower_name: [
        this.elementData.element?.borrower_name,
        Validators.required,
      ],
      available_stocks: [this.elementData.element?.item_quantity],
      item_brand: [this.elementData.element?.item_brand, Validators.required],
      item_unit: [this.elementData.element?.item_unit, Validators.required],
      item_quantity: ['', Validators.required],
      item_category: [''],
      item_reason: [''],
    });
    // this.createform.controls['borrower_name'].disable();
    // this.createform.controls['item_quantity'].disable();
    // ato fetch
    this.getAllComlab();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const target_value = parseInt(this.createform.get('item_quantity').value);
      const available_stocks = parseInt(
        this.createform.get('available_stocks').value
      );

      console.log('item quantity', target_value);
      console.log('Test', available_stocks);

      if (target_value > available_stocks) {
        this.toastr.error('You cannot borow more than the available stocks.');
      } else {
        const available_stock = parseInt(
          this.elementData.element.item_quantity
        );
        const borrowed_quantity = parseInt(
          this.createform.get('item_quantity').value
        );

        console.log(this.elementData.element);

        const data = {
          id: this.elementData.element.id,
          item_id: this.elementData.element.item_id,
          item_name: this.createform.get('item_name').value,
          borrower_name: this.createform.get('borrower_name').value,
          item_brand: this.createform.get('item_brand').value,
          item_unit: this.createform.get('item_unit').value,
          item_quantity: this.createform.get('item_quantity').value,
          item_category: this.createform.get('item_category').value,
          module: this.elementData.element.module,
          update_quantity: available_stock - borrowed_quantity,
          update_room_items: this.elementData.element.item_stocks,
          reason: this.createform.get('item_reason').value,
        };

        console.log('data', data);

        this.SchoolPropertyService.createReturnItems(data).subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('Success Return item.');
            this.dialogRef.close();
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );
      }
    }
  }

  //Borrow Items

  getAllComlab() {
    this.SchoolPropertyService.getAllComlab(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;
      this.itemArr = result.data.data;
    });
  }
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { BorrowItemBuildingComponent } from 'src/app/school-property/building/show-building-list/borrow-item-building/borrow-item-building.component';
import { SchoolPropertyService } from 'src/app/school-property/shared/school-property.service';
import { ShowRoomListComponent } from '../show-room-list.component';

@Component({
  selector: 'app-borrow-item-rooms',
  templateUrl: './borrow-item-rooms.component.html',
  styleUrls: ['./borrow-item-rooms.component.scss'],
})
export class BorrowItemRoomsComponent implements OnInit {
  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  itemArr: any = [];

  createform: FormGroup;

  elementData: any = [];

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<ShowRoomListComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.elementData = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: [this.elementData.element?.item_name, Validators.required],
      borrower_name: ['', Validators.required],
      available_stocks: [this.elementData.element?.item_quantity],
      item_brand: [this.elementData.element?.item_brand, Validators.required],
      item_unit: [this.elementData.element?.item_unit, Validators.required],
      item_quantity: ['', Validators.required],
      // item_category: [
      //   this.elementData.element?.item_category,
      //   Validators.required,
      // ],
    });
    // this.createform.controls['borrower_name'].disable();
    // this.createform.controls['item_quantity'].disable();
    // ato fetch
    this.getAllComlab();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const target_value = parseInt(this.createform.get('item_quantity').value);
      const stock_available = parseInt(this.elementData.element.item_quantity);

      console.log(target_value);
      console.log(stock_available);

      if (target_value > stock_available) {
        this.toastr.error('You cannot borow more than the available stocks.');
      } else {
        const available_stock = parseInt(
          this.elementData.element.item_quantity
        );
        const borrowed_quantity = parseInt(
          this.createform.get('item_quantity').value
        );

        const data = {
          id: this.elementData.element.id,
          item_name: this.createform.get('item_name').value,
          borrower_name: this.createform.get('borrower_name').value,
          item_brand: this.createform.get('item_brand').value,
          item_unit: this.createform.get('item_unit').value,
          item_quantity: this.createform.get('item_quantity').value,
          item_category: this.elementData.element.item_category,
          module: 'Rooms',
          update_quantity: available_stock - borrowed_quantity,
        };

        console.log('Stock In', data);

        this.SchoolPropertyService.createBorrowItems(data).subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('You have successfully borrow an item.');
            this.dialogRef.close();
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );
      }
    }
  }

  //Borrow Items

  getAllComlab() {
    this.SchoolPropertyService.getAllComlab(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;
      this.itemArr = result.data.data;
    });
  }

  onChangeQuantity(event) {}
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { DeleteLaboratoryComponent } from 'src/app/school-property/laboratory/delete-laboratory/delete-laboratory.component';
import { SchoolPropertyService } from 'src/app/school-property/shared/school-property.service';

@Component({
  selector: 'app-delete-room-list',
  templateUrl: './delete-room-list.component.html',
  styleUrls: ['./delete-room-list.component.scss'],
})
export class DeleteRoomListComponent implements OnInit {
  id: any;

  constructor(
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<DeleteLaboratoryComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {}

  onSubmit() {
    const id = this.id.id;
    this.SchoolPropertyService.deleteSingleRoomItem(id).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success('Successfully Deleted');
        this.dialogRef.close();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }
}

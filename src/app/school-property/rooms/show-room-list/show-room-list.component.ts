import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { CreateRoomsComponent } from '../create-rooms/create-rooms.component';
import { RoomsComponent } from '../rooms.component';
import { CreateRoomItemsComponent } from './create-room-items/create-room-items.component';
import { BorrowItemRoomsComponent } from './borrow-item-rooms/borrow-item-rooms.component';
import { EditRoomItemsComponent } from './edit-room-items/edit-room-items.component';
import { DeleteRoomListComponent } from './delete-room-list/delete-room-list.component';

@Component({
  selector: 'app-show-room-list',
  templateUrl: './show-room-list.component.html',
  styleUrls: ['./show-room-list.component.scss'],
})
export class ShowRoomListComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];
  id: any;

  displaycolumn: string[] = [
    'number',
    'model_no',
    'room_id',
    'item_name',
    'item_quantity',
    'item_brand',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<RoomsComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }
  ngOnInit() {
    this.getAllRoomItems();
  }

  createRoom() {
    const id = this.id.id;
    const dialogRef = this.dialog.open(CreateRoomItemsComponent, {
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRoomItems();
      console.log('The dialog was closed', result);
    });
  }

  showItemList(id) {
    // const dialogRef = this.dialog.open(ShowRoomListComponent, {
    //   // panelClass: 'app-full-bleed-dialog-p-10',
    //   data: {
    //     id,
    //   },
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   this.getAllRooms();
    //   console.log('The dialog was closed', result);
    // });
  }

  onclickEdit(element) {
    const dialogRef = this.dialog.open(EditRoomItemsComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRoomItems();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteRoomListComponent, {
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRoomItems();
      console.log('The dialog was closed', result);
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllRoomItems();
  }

  getAllRoomItems() {
    const id = this.id.id;
    this.SchoolPropertyService.getAllRoomItems(this.paginate, id).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllRoomItems()
      : this.paginator.firstPage();
  }

  //Borrow
  onclickBorrow(element) {
    const dialogRef = this.dialog.open(BorrowItemRoomsComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRoomItems();
      console.log('The dialog was closed', result);
    });
  }
}

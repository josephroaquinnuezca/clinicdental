import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-rooms',
  templateUrl: './edit-rooms.component.html',
  styleUrls: ['./edit-rooms.component.scss'],
})
export class EditRoomsComponent implements OnInit {
  createform: FormGroup;
  id: any;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<EditRoomsComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      room_name: ['', Validators.required],
      room_description: ['', Validators.required],
    });

    this.getSingleRooms();
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const id = this.id.id;
      const data = {
        room_name: this.createform.get('room_name').value,
        room_description: this.createform.get('room_description').value,
      };

      this.SchoolPropertyService.updateSingRooms(data, id).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }

  // auto prefil data
  getSingleRooms() {
    this.SchoolPropertyService.getSingleRooms(this.id.id).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.createform.get('room_name').patchValue(result.data.room_name);
      this.createform
        .get('room_description')
        .patchValue(result.data.room_description);
    });
  }
}

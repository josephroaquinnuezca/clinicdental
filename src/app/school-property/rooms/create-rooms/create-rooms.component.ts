import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-rooms',
  templateUrl: './create-rooms.component.html',
  styleUrls: ['./create-rooms.component.scss'],
})
export class CreateRoomsComponent implements OnInit {
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<CreateRoomsComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {}

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      room_name: ['', Validators.required],
      room_description: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        room_name: this.createform.get('room_name').value,
        room_description: this.createform.get('room_description').value,
      };

      this.SchoolPropertyService.createRooms(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            if (
              err.error.description ===
              'SequelizeUniqueConstraintError: Validation error'
            ) {
              this.toastr.error('Room name is already exist');
            }
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

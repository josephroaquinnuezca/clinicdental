import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-delete-rooms',
  templateUrl: './delete-rooms.component.html',
  styleUrls: ['./delete-rooms.component.scss'],
})
export class DeleteRoomsComponent implements OnInit {
  id: any;

  constructor(
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<DeleteRoomsComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {}
  onSubmit() {
    const id = this.id.id;
    this.SchoolPropertyService.deleteSingleIdRooms(id).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success('Successfully Deleted');
        this.dialogRef.close();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from '../shared/school-property.service';
import { CreateRoomsComponent } from './create-rooms/create-rooms.component';
import { EditRoomsComponent } from './edit-rooms/edit-rooms.component';
import { DeleteRoomsComponent } from './delete-rooms/delete-rooms.component';
import { ShowRoomListComponent } from './show-room-list/show-room-list.component';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0,
        })
      ),
      transition('void <=> *', animate(300)),
    ]),
  ],
})
export class RoomsComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'room_name',
    'room_description',
    'items',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllRooms();
  }

  createRoom() {
    const dialogRef = this.dialog.open(CreateRoomsComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRooms();
      console.log('The dialog was closed', result);
    });
  }

  showItemList(id) {
    const dialogRef = this.dialog.open(ShowRoomListComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRooms();
      console.log('The dialog was closed', result);
    });
  }

  onclickEdit(id) {
    const dialogRef = this.dialog.open(EditRoomsComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllRooms();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteRoomsComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllRooms();
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllRooms();
  }

  getAllRooms() {
    this.SchoolPropertyService.getAllRooms(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;

      this.getAllData = result.data.data;
      this.paginate.totalCount = result.data.totalCount;
      this.datasoure = new MatTableDataSource<any>(this.getAllData);
      this.datasoure.data = this.getAllData;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllRooms()
      : this.paginator.firstPage();
  }
}

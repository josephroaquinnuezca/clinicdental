import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { LaboratoryComponent } from '../laboratory.component';

@Component({
  selector: 'app-creae-laboratory',
  templateUrl: './creae-laboratory.component.html',
  styleUrls: ['./creae-laboratory.component.scss'],
})
export class CreaeLaboratoryComponent implements OnInit {
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<LaboratoryComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {}

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: ['', Validators.required],
      item_brand: ['', Validators.required],
      item_unit: ['', Validators.required],
      item_quantity: ['', Validators.required],
      item_category: ['', Validators.required],
      model_no: ['', Validators.required],
    });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        item_name: this.createform.get('item_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_category: this.createform.get('item_category').value,
        model_no: this.createform.get('model_no').value,
      };

      console.log('Custodian', data);

      this.SchoolPropertyService.createComLab(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully added new Item.');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

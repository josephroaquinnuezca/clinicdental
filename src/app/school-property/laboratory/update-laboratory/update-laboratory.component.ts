import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { LaboratoryComponent } from '../laboratory.component';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-update-laboratory',
  templateUrl: './update-laboratory.component.html',
  styleUrls: ['./update-laboratory.component.scss'],
})
export class UpdateLaboratoryComponent implements OnInit {
  id: any;
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<UpdateLaboratoryComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: ['', Validators.required],
      item_brand: ['', Validators.required],
      item_unit: ['', Validators.required],
      item_quantity: ['', Validators.required],
      item_category: ['', Validators.required],
    });
    this.getSingleComlab();
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        id: this.id.id,
        item_name: this.createform.get('item_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_category: this.createform.get('item_category').value,
      };

      console.log('Comlab', data);

      this.SchoolPropertyService.updateSingleComlab(data, this.id.id).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }

  // auto prefil data
  getSingleComlab() {
    this.SchoolPropertyService.getSingleComlab(this.id.id).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.createform.get('item_name').patchValue(result.data.item_name);
      this.createform
        .get('item_quantity')
        .patchValue(result.data.item_quantity);
      this.createform.get('item_brand').patchValue(result.data.item_brand);
      this.createform.get('item_unit').patchValue(result.data.item_unit);
      this.createform
        .get('item_category')
        .patchValue(result.data.item_category);
    });
  }
}

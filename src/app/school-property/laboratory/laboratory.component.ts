import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UsersCreateComponent } from '../users-account/users-create/users-create.component';
import { MatDialog } from '@angular/material/dialog';
import { SchoolPropertyService } from '../shared/school-property.service';
import { CreaeLaboratoryComponent } from './creae-laboratory/creae-laboratory.component';
import { BorrowItemsComponent } from './borrow-items/borrow-items.component';
import { UpdateLaboratoryComponent } from './update-laboratory/update-laboratory.component';
import { ShowBuildingListComponent } from '../building/show-building-list/show-building-list.component';
import { BorrowItemBuildingComponent } from '../building/show-building-list/borrow-item-building/borrow-item-building.component';
import { BorrowItemLaboratoryComponent } from './borrow-item-laboratory/borrow-item-laboratory.component';
import { DeleteLaboratoryComponent } from './delete-laboratory/delete-laboratory.component';

@Component({
  selector: 'app-laboratory',
  templateUrl: './laboratory.component.html',
  styleUrls: ['./laboratory.component.scss'],
})
export class LaboratoryComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  isChecked: boolean = false;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'model_no',
    'item_name',
    'item_quantity',
    'item_brand',
    'item_unit',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllComlab();
  }

  onToggleChange() {
    // Handle the toggle change event here
    console.log('Toggle switched:', this.isChecked);
  }

  createRoom() {
    const dialogRef = this.dialog.open(CreaeLaboratoryComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }

  borrowItems() {
    const dialogRef = this.dialog.open(BorrowItemsComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }

  onclickEdit(id) {
    const dialogRef = this.dialog.open(UpdateLaboratoryComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteLaboratoryComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllComlab();
  }

  getAllComlab() {
    this.SchoolPropertyService.getAllComlab(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;

      this.getAllData = result.data.data;
      this.paginate.totalCount = result.data.totalCount;
      this.datasoure = new MatTableDataSource<any>(this.getAllData);
      this.datasoure.data = this.getAllData;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllComlab()
      : this.paginator.firstPage();
  }

  // 07/03/2024
  showItemList(id) {
    const dialogRef = this.dialog.open(ShowBuildingListComponent, {
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }

  //Borrow
  onclickBorrow(element) {
    const dialogRef = this.dialog.open(BorrowItemLaboratoryComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllComlab();
      console.log('The dialog was closed', result);
    });
  }
}

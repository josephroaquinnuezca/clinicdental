import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { LaboratoryComponent } from '../laboratory.component';
import { SchoolPropertyService } from '../../shared/school-property.service';

@Component({
  selector: 'app-borrow-items',
  templateUrl: './borrow-items.component.html',
  styleUrls: ['./borrow-items.component.scss'],
})
export class BorrowItemsComponent implements OnInit {
  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  itemArr: any = [];

  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<LaboratoryComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {}

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: ['', Validators.required],
      borrower_name: ['', Validators.required],
      available_stocks: [''],
      item_brand: ['', Validators.required],
      item_unit: ['', Validators.required],
      item_quantity: ['', [Validators.required]],
      item_category: ['', Validators.required],
    });
    this.createform.controls['borrower_name'].disable();
    this.createform.controls['item_quantity'].disable();
    // ato fetch
    this.getAllComlab();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        item_id: this.createform.get('item_name').value.id,
        item_name: this.createform.get('item_name').value.item_name,
        borrower_name: this.createform.get('borrower_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_category: this.createform.get('item_category').value,
      };

      console.log('Borrow Data', data);

      this.SchoolPropertyService.createBorrowItems(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Added Data');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }

  //Borrow Items

  getAllComlab() {
    this.SchoolPropertyService.getAllComlab(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;
      this.itemArr = result.data.data;
    });
  }

  //Onchange Eventt
  onChangeItem(event) {
    this.SchoolPropertyService.getSingleComlab(event.value.id).subscribe(
      (data) => {
        this.createform.controls['borrower_name'].enable();
        this.createform.controls['item_quantity'].enable();
        console.log('data', data);
        var result: any = data;
        this.createform.get('item_brand').patchValue(result.data.item_brand);
        this.createform.get('item_unit').patchValue(result.data.item_unit);
        this.createform
          .get('available_stocks')
          .patchValue(result.data.item_quantity);
        this.createform
          .get('item_category')
          .patchValue(result.data.item_category);

        console.log('data', result.data.item_category);
      }
    );
  }

  onChangeQuantity(event) {}
}

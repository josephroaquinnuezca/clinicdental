import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { SchoolPropertyService } from '../shared/school-property.service';
import {
  ArcElement,
  BarController,
  BarElement,
  BubbleController,
  CategoryScale,
  Chart,
  Decimation,
  DoughnutController,
  Filler,
  Legend,
  LineController,
  LineElement,
  LinearScale,
  LogarithmicScale,
  PieController,
  PointElement,
  PolarAreaController,
  RadarController,
  RadialLinearScale,
  ScatterController,
  SubTitle,
  TimeScale,
  TimeSeriesScale,
  Title,
  Tooltip,
} from 'chart.js';

@Component({
  selector: 'app-school-dashboard',
  templateUrl: './school-dashboard.component.html',
  styleUrls: ['./school-dashboard.component.scss'],
})
export class SchoolDashboardComponent implements OnInit {
  currentyear: any = [];
  rooms: any = [];
  stockin: any = [];
  stockout: any = [];
  total: any = [];
  myDate = new Date(Date.now());

  constructor(public SchoolPropertyService: SchoolPropertyService) {
    Chart.register(
      ArcElement,
      LineElement,
      BarElement,
      PointElement,
      BarController,
      BubbleController,
      DoughnutController,
      LineController,
      PieController,
      PolarAreaController,
      RadarController,
      ScatterController,
      CategoryScale,
      LinearScale,
      LogarithmicScale,
      RadialLinearScale,
      TimeScale,
      TimeSeriesScale,
      Decimation,
      Filler,
      Legend,
      Title,
      Tooltip,
      SubTitle
    );
  }

  ngOnInit(): void {
    this.currentyear = new Date().getFullYear();

    const date = moment(this.myDate).format('l');

    this.SchoolPropertyService.getAllReports(date).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.rooms = result.data.result2['0'].roomNo;

      console.log(date);
      // const testA: number = this.rooms.reduce((acc, item) => {
      //   return acc + item.item_quantity;
      // }, 0);

      // this.total = testA;
      // console.log(this.rooms);
      // for (let j = 0; j < this.rooms.length; j++) {
      //   this.total += this.rooms.item_quantity;
      //   console.log(this.total);
      // }
    });
    this.pieChartFunction(date);
  }

  pieChartFunction(date) {
    const DATA_COUNT = 2;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('piechart');

    this.SchoolPropertyService.getAllReports(date).subscribe((data) => {
      var result: any = data;
      this.stockin = result.data.total_stock_in['0'].stock_in;
      this.stockout = result.data.total_stock_out['0'].stock_out;

      var myChart = new Chart('piechart', {
        type: 'pie',
        data: {
          labels: ['Stock In', 'Stock Out'],
          datasets: [
            {
              label: 'Dataset 1',
              data: [this.stockin, this.stockout],
              backgroundColor: ['#05445E', '#189AB4'],
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Stock In and Stock Out',
            },
          },
        },
      });
    }); //end
  } //end functio
}

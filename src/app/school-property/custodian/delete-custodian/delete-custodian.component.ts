import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { DeleteBuildingComponent } from '../../building/delete-building/delete-building.component';
import { SchoolPropertyService } from '../../shared/school-property.service';

@Component({
  selector: 'app-delete-custodian',
  templateUrl: './delete-custodian.component.html',
  styleUrls: ['./delete-custodian.component.scss'],
})
export class DeleteCustodianComponent implements OnInit {
  id: any;

  constructor(
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<DeleteBuildingComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {}

  onSubmit() {
    const id = this.id.id;
    this.SchoolPropertyService.deleteSingleCustodian(id).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success('Successfully Deleted');
        this.dialogRef.close();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }
}

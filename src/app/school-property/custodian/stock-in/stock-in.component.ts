import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { CustodianComponent } from '../custodian.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stock-in',
  templateUrl: './stock-in.component.html',
  styleUrls: ['./stock-in.component.scss'],
})
export class StockInComponent implements OnInit {
  createform: FormGroup;

  elementData: any = [];
  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<StockInComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.elementData = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: [this.elementData.element.item_name, Validators.required],
      item_description: [
        this.elementData.element.item_description,
        Validators.required,
      ],
      item_brand: [this.elementData.element.item_brand, Validators.required],
      item_unit: [this.elementData.element.item_unit, Validators.required],
      item_quantity: ['', Validators.required],
      item_category: [
        this.elementData.element.item_category,
        Validators.required,
      ],
    });
  }

  get f() {
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const number1 = parseInt(this.elementData.element.item_quantity);
      const number2 = parseInt(this.createform.get('item_quantity').value);

      const data = {
        item_id: this.elementData.element.id,
        item_name: this.createform.get('item_name').value,
        item_description: this.createform.get('item_description').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_category: this.createform.get('item_category').value,
        sum_of_quantity: number1 + number2,
      };

      console.log('Stock In', data);

      this.SchoolPropertyService.createStockIn(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Added');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

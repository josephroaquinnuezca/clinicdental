import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-stocks-mgnt',
  templateUrl: './stocks-mgnt.component.html',
  styleUrls: ['./stocks-mgnt.component.scss'],
})
export class StocksMgntComponent implements OnInit {
 
  constructor(
  ) {}

  ngOnInit() {
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from 'src/app/school-property/shared/school-property.service';
import { ViewListStockOutComponent } from '../../view-list-stock-out/view-list-stock-out.component';

@Component({
  selector: 'app-stockout-mngt',
  templateUrl: './stockout-mngt.component.html',
  styleUrls: ['./stockout-mngt.component.scss'],
})
export class StockoutMngtComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'item_name',
    'item_quantity',
    'item_brand',
    'item_category',
    'item_unit',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllStockOut();
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllStockOut();
  }

  getAllStockOut() {
    this.SchoolPropertyService.getAllStockOut(this.paginate).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllStockOut()
      : this.paginator.firstPage();
  }

  onclickViewStockOut(element) {
    const dialogRef = this.dialog.open(ViewListStockOutComponent, {
      data: {
        element,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllStockOut();
      console.log('The dialog was closed', result);
    });
  }
}

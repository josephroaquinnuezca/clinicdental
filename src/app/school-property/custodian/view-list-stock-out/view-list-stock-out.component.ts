import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-list-stock-out',
  templateUrl: './view-list-stock-out.component.html',
  styleUrls: ['./view-list-stock-out.component.scss'],
})
export class ViewListStockOutComponent implements OnInit {
  elementData: any = [];

  constructor(@Inject(MAT_DIALOG_DATA) public results) {
    this.elementData = results;
  }

  ngOnInit(): void {}
}

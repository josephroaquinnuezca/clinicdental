import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { SchoolPropertyService } from '../shared/school-property.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { CreateCustodianComponent } from './create-custodian/create-custodian.component';
import { EditCustodianComponent } from './edit-custodian/edit-custodian.component';
import { DeleteCustodianComponent } from './delete-custodian/delete-custodian.component';
import { StockInComponent } from './stock-in/stock-in.component';
import { StockOutComponent } from './stock-out/stock-out.component';
import { ViewListStockOutComponent } from './view-list-stock-out/view-list-stock-out.component';

@Component({
  selector: 'app-custodian',
  templateUrl: './custodian.component.html',
  styleUrls: ['./custodian.component.scss'],
})
export class CustodianComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'item_name',
    'item_quantity',
    'item_brand',
    'item_category',
    'item_unit',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllCustodian();
  }

  createRoom() {
    const dialogRef = this.dialog.open(CreateCustodianComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onclickEdit(id) {
    const dialogRef = this.dialog.open(EditCustodianComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteCustodianComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onclickStockIn(element) {
    const dialogRef = this.dialog.open(StockInComponent, {
      data: {
        element,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onclickStockOut(element) {
    const dialogRef = this.dialog.open(StockOutComponent, {
      data: {
        element,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onclickViewStockOut(element) {
    const dialogRef = this.dialog.open(ViewListStockOutComponent, {
      data: {
        element,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllCustodian();
      console.log('The dialog was closed', result);
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllCustodian();
  }

  getAllCustodian() {
    this.SchoolPropertyService.getAllCustodian(this.paginate).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllCustodian()
      : this.paginator.firstPage();
  }
}

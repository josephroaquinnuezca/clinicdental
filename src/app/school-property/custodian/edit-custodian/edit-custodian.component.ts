import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CustodianComponent } from '../custodian.component';

@Component({
  selector: 'app-edit-custodian',
  templateUrl: './edit-custodian.component.html',
  styleUrls: ['./edit-custodian.component.scss'],
})
export class EditCustodianComponent implements OnInit {
  id: any;
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<EditCustodianComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: ['', Validators.required],
      item_quantity: ['', Validators.required],
      item_brand: ['', Validators.required],
      item_unit: ['', Validators.required],
      item_category: ['', Validators.required],
    });
    console.log('Dialog id', this.id.id);
    this.getSingleCustodian();
  }

  getSingleCustodian() {
    this.SchoolPropertyService.getSingleCustodian(this.id.id).subscribe(
      (data) => {
        console.log('data', data);
        var result: any = data;
        this.createform.get('item_name').patchValue(result.data.item_name);
        this.createform
          .get('item_quantity')
          .patchValue(result.data.item_quantity);
        this.createform.get('item_brand').patchValue(result.data.item_brand);
        this.createform.get('item_unit').patchValue(result.data.item_unit);
        this.createform
          .get('item_category')
          .patchValue(result.data.item_category);
      }
    );
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        item_name: this.createform.get('item_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_category: this.createform.get('item_category').value,
      };

      const id = this.id.id;

      this.SchoolPropertyService.updateSingleCustodian(data, id).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

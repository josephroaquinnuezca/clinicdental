import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CustodianComponent } from '../custodian.component';

@Component({
  selector: 'app-create-custodian',
  templateUrl: './create-custodian.component.html',
  styleUrls: ['./create-custodian.component.scss'],
})
export class CreateCustodianComponent implements OnInit {
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<CustodianComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {}

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: ['', Validators.required],
      item_description: ['', Validators.required],
      item_brand: ['', Validators.required],
      item_unit: ['', Validators.required],
      // item_quantity: ['', [Validators.required, Validators.maxLength(3)]],
      item_category: ['', Validators.required],
    });
  }

  get f() {
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        item_name: this.createform.get('item_name').value,
        item_description: this.createform.get('item_description').value,
        item_brand: this.createform.get('item_brand').value,
        item_unit: this.createform.get('item_unit').value,
        item_quantity: 0,
        item_category: this.createform.get('item_category').value,
      };

      console.log('Custodian', data);

      this.SchoolPropertyService.createCustodian(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP_API_URL } from '../../_shared/api-service/api-service.service';
import { Observable } from 'rxjs';
import { DayTableSlicer } from '@fullcalendar/daygrid';

@Injectable({
  providedIn: 'root',
})
export class SchoolPropertyService {
  constructor(private http: HttpClient) {}

  getAllStatusMngt(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-status-mngt/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllStockIn(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-stock-in/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllStockOut(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-stock-out/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  // getAllStocks(data: any) {
  //   return this.http.get(
  //     `${HTTP_API_URL}get-all-stock/` +
  //       `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
  //   );
  // }

  getAllSystemLogs(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-system-logs/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllRoomItems(data: any, id) {
    return this.http.get(
      `${HTTP_API_URL}get-all-room-items/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&id=${id}`
    );
  }

  getAllBuildingItems(data: any, id) {
    return this.http.get(
      `${HTTP_API_URL}get-all-building-items/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&id=${id}`
    );
  }

  getAllBorrowItems(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-borrow-items/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllBuilding(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-building/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllComlab(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-comlab/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllRooms(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-rooms/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  getAllCustodian(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-custodian/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }
  getAllUsers(data: any) {
    return this.http.get(
      `${HTTP_API_URL}get-all-users/` +
        `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`
    );
  }

  // add function

  CreateUsers(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-users`;
    return this.http.post(API_URL, data);
  }

  createCustodian(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-custodian`;
    return this.http.post(API_URL, data);
  }

  createComLab(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-comlab`;
    return this.http.post(API_URL, data);
  }

  createRooms(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-rooms`;
    return this.http.post(API_URL, data);
  }

  createBuilding(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-building`;
    return this.http.post(API_URL, data);
  }

  createBorrowItems(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-borrow-items`;
    return this.http.post(API_URL, data);
  }

  createReturnItems(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-return-items`;
    return this.http.post(API_URL, data);
  }

  createRoomItem(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-room-item`;
    return this.http.post(API_URL, data);
  }

  createBuildingItem(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-building-item`;
    return this.http.post(API_URL, data);
  }

  createStockIn(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-stock-in`;
    return this.http.post(API_URL, data);
  }

  createStockOut(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-stock-out`;
    return this.http.post(API_URL, data);
  }

  // GET ALL SINGLE DAA
  getSingleUsers(id) {
    return this.http.get(`${HTTP_API_URL}get-single-users/${id}`);
  }

  getSingleCustodian(id: any) {
    return this.http.get(`${HTTP_API_URL}/get-single-custodian` + `?id=${id}`);
  }

  getSingleComlab(id: any) {
    return this.http.get(`${HTTP_API_URL}/get-single-comlab` + `?id=${id}`);
  }

  getSingleRooms(id: any) {
    return this.http.get(`${HTTP_API_URL}/get-single-rooms` + `?id=${id}`);
  }

  getSingleBuilding(id: any) {
    return this.http.get(`${HTTP_API_URL}/get-single-building` + `?id=${id}`);
  }

  //UPDATE FUNCTION
  updateUserStatus(id: any, data: any) {
    return this.http.get(
      `${HTTP_API_URL}/update-user-status` + `?id=${id}&data=${data}`
    );
  }
  updateSingleCustodian(data: any, id): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-custodian/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }
  updateSingleComlab(data: any, id): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-comlab/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }

  updateSingRooms(data: any, id): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-rooms/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }

  updateSingleBuilding(data: any, id): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-building/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }

  updateSingBuildingItem(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-building-item/`;
    return this.http.post(API_URL, data);
  }

  updateSingRoomItem(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-single-room-item/`;
    return this.http.post(API_URL, data);
  }

  updateUsers(data: any, id: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-users/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }

  // DELETE FUNCTION
  deleteSingleIdRooms(id: any) {
    return this.http.get(`${HTTP_API_URL}/delete-single-rooms` + `?id=${id}`);
  }

  deleteSingleBuilding(id: any) {
    return this.http.get(
      `${HTTP_API_URL}/delete-single-building` + `?id=${id}`
    );
  }

  deleteSingleCustodian(id: any) {
    return this.http.get(
      `${HTTP_API_URL}/delete-single-custodian` + `?id=${id}`
    );
  }

  deleteSingleLaboratory(id: any) {
    return this.http.get(
      `${HTTP_API_URL}/delete-single-laboratory` + `?id=${id}`
    );
  }

  deleteSingleRoomItem(id: any) {
    return this.http.get(
      `${HTTP_API_URL}/delete-single-room-item` + `?id=${id}`
    );
  }

  deleteSingleBuidingItem(id: any) {
    return this.http.get(
      `${HTTP_API_URL}/delete-single-building-item` + `?id=${id}`
    );
  }

  getAllReports(date: any) {
    return this.http.get(`${HTTP_API_URL}/get-all-reports` + `?date=${date}`);
  }

  getAllReprtBydateRange(start_date, end_date) {
    return this.http.get(
      `${HTTP_API_URL}/get-all-reports-date-range` +
        `?start_date=${start_date}&end_date=${end_date}`
    );
  }
}

import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { SchoolPropertyService } from '../shared/school-property.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-school-reports',
  templateUrl: './school-reports.component.html',
  styleUrls: ['./school-reports.component.scss'],
})
export class SchoolReportsComponent implements OnInit {
  rooms: any = [];
  stock_in: any = [];
  stock_out: any = [];
  total: any = [];
  borrow_items: any = [];

  range_result1: any = [];
  range_result2: any = [];
  range_result3: any = [];
  range_result4: any = [];

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  currentTabIndex = 0;

  constructor(public SchoolPropertyService: SchoolPropertyService) {}

  ngOnInit(): void {}

  onTabChange(event: any) {
    // this.currentTabIndex = event.index;

    console.log('tab change', event.index);
    if (event.index == 1) {
      this.currentTabIndex = 1;
    } else if (event.index == 2) {
      this.currentTabIndex = 1;
    } else if (event.index == 3) {
      this.currentTabIndex = 1;
    } else {
      this.currentTabIndex = 0;
    }
  }

  dateRangeChange(
    dateRangeStart: HTMLInputElement,
    dateRangeEnd: HTMLInputElement
  ) {
    console.log(dateRangeStart.value);
    console.log(dateRangeEnd.value);

    const start_date = moment(dateRangeStart.value).format('l');
    const end_date = moment(dateRangeEnd.value).format('l');

    console.log('date', start_date);

    this.SchoolPropertyService.getAllReprtBydateRange(
      start_date,
      end_date
    ).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.range_result1 = result.data.result1;
      this.range_result2 = result.data.result2;
      this.range_result3 = result.data.result3;
      this.range_result4 = result.data.result4;
    });
  }

  onChangeEventUsers(event: any) {
    const dateModify = moment(event.target.value).format('l');

    console.log('date', dateModify);

    this.SchoolPropertyService.getAllReports(dateModify).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.rooms = result.data.result1;
      this.stock_in = result.data.stockin;
      this.stock_out = result.data.stockout;
      this.borrow_items = result.data.borrow_items;
      // const testA: number = this.rooms.reduce((acc, item) => {
      //   return acc + item.item_quantity;
      // }, 0);

      // this.total = testA;
      // console.log(this.rooms);
      // for (let j = 0; j < this.rooms.length; j++) {
      //   this.total += this.rooms.item_quantity;
      //   console.log(this.total);
      // }
    });
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage-property',
  templateUrl: './homepage-property.component.html',
  styleUrls: ['./homepage-property.component.scss'],
})
export class HomepagePropertyComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  scrollToContact($element): void {
    console.log($element);
    $element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  gototop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }
}

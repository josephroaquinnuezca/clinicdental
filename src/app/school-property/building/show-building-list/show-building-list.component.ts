import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { CreateBuildingListComponent } from './create-building-list/create-building-list.component';
import { BuildingComponent } from '../building.component';
import { BorrowItemsComponent } from '../../laboratory/borrow-items/borrow-items.component';
import { BorrowItemBuildingComponent } from './borrow-item-building/borrow-item-building.component';
import { UpdateRoomItemsComponent } from './update-room-items/update-room-items.component';
import { DeleteItemBuildingComponent } from './delete-item-building/delete-item-building.component';

@Component({
  selector: 'app-show-building-list',
  templateUrl: './show-building-list.component.html',
  styleUrls: ['./show-building-list.component.scss'],
})
export class ShowBuildingListComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  id: any;

  displaycolumn: string[] = [
    'number',
    'model_no',
    'room_id',
    'item_name',
    'item_quantity',
    'item_brand',
    'item_date',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<ShowBuildingListComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }
  ngOnInit() {
    this.getAllBuildingItems();
  }
  createRoom() {
    const id = this.id.id;
    const dialogRef = this.dialog.open(CreateBuildingListComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuildingItems();
      console.log('The dialog was closed', result);
    });
  }

  showItemList(id) {
    // const dialogRef = this.dialog.open(ShowRoomListComponent, {
    //   // panelClass: 'app-full-bleed-dialog-p-10',
    //   data: {
    //     id,
    //   },
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   this.getAllRooms();
    //   console.log('The dialog was closed', result);
    // });
  }

  onclickEdit(element) {
    const dialogRef = this.dialog.open(UpdateRoomItemsComponent, {
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuildingItems();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteItemBuildingComponent, {
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllBuildingItems();
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllBuildingItems();
  }

  getAllBuildingItems() {
    const id = this.id.id;
    this.SchoolPropertyService.getAllBuildingItems(this.paginate, id).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllBuildingItems()
      : this.paginator.firstPage();
  }

  //Borrow
  onclickBorrow(element) {
    const dialogRef = this.dialog.open(BorrowItemBuildingComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuildingItems();
      console.log('The dialog was closed', result);
    });
  }
}

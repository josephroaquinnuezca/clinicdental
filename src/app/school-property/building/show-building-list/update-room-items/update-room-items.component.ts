import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from 'src/app/school-property/shared/school-property.service';
import { ShowBuildingListComponent } from '../show-building-list.component';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-update-room-items',
  templateUrl: './update-room-items.component.html',
  styleUrls: ['./update-room-items.component.scss'],
})
export class UpdateRoomItemsComponent implements OnInit {
  createform: FormGroup;
  id: any;

  elementData: any = [];

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<ShowBuildingListComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.elementData = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      item_name: [this.elementData.element?.item_name, Validators.required],
      item_brand: [this.elementData.element?.item_brand, Validators.required],
      item_quantity: [
        this.elementData.element?.item_quantity,
        Validators.required,
      ],
      item_unit: [this.elementData.element?.item_unit, Validators.required],
      item_category: [
        this.elementData.element?.item_category,
        Validators.required,
      ],
    });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        id: this.elementData.element?.id,
        item_name: this.createform.get('item_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_unit: this.createform.get('item_unit').value,
        item_category: this.createform.get('item_category').value,
      };

      console.log(data);

      this.SchoolPropertyService.updateSingBuildingItem(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

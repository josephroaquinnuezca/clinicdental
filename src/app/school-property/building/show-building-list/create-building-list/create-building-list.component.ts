import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from 'src/app/school-property/shared/school-property.service';
import { ShowBuildingListComponent } from '../show-building-list.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-building-list',
  templateUrl: './create-building-list.component.html',
  styleUrls: ['./create-building-list.component.scss'],
})
export class CreateBuildingListComponent implements OnInit {
  createform: FormGroup;
  id: any;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<ShowBuildingListComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      model_no: ['', Validators.required],
      item_name: ['', Validators.required],
      item_brand: ['', Validators.required],
      item_quantity: ['', Validators.required],
      item_unit: ['', Validators.required],
      item_category: ['', Validators.required],
    });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        room_id: this.id.id,
        item_name: this.createform.get('item_name').value,
        item_brand: this.createform.get('item_brand').value,
        item_quantity: this.createform.get('item_quantity').value,
        item_unit: this.createform.get('item_unit').value,
        item_category: this.createform.get('item_category').value,
        model_no: this.createform.get('model_no').value,
      };

      this.SchoolPropertyService.createBuildingItem(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

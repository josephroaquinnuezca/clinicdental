import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-delete-building',
  templateUrl: './delete-building.component.html',
  styleUrls: ['./delete-building.component.scss']
})
export class DeleteBuildingComponent implements OnInit {

  id: any;

  constructor(
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<DeleteBuildingComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {}

  onSubmit() {
    const id = this.id.id;
    this.SchoolPropertyService.deleteSingleBuilding(id).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success('Successfully Deleted');
        this.dialogRef.close();
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }
}

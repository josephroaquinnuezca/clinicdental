import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmationDeleteDialogComponent } from 'src/app/_resusesable-dialogs/confirmation-delete-dialog/confirmation-delete-dialog.component';
import { AppointServiceService } from 'src/app/_shared/shared/appoint-service.service';
import { EditDialogComponent } from 'src/app/doctor-routes/manage-appointment/edit-dialog/edit-dialog.component';
import { ConfirmSchedDialogComponent } from 'src/app/doctor-routes/manage-client-schedule/confirm-sched-dialog/confirm-sched-dialog.component';
import { WalkinUserDialogComponent } from 'src/app/doctor-routes/manage-client-schedule/walkin-user-dialog/walkin-user-dialog.component';
import { SchoolPropertyService } from '../shared/school-property.service';
import { CreaeBuildingComponent } from './creae-building/creae-building.component';
import { DeleteBuildingComponent } from './delete-building/delete-building.component';
import { EditBuildingComponent } from './edit-building/edit-building.component';
import { ShowBuildingListComponent } from './show-building-list/show-building-list.component';

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.scss'],
})
export class BuildingComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'room_name',
    'room_description',
    'room_floor',
    'items',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllBuilding();
  }

  createRoom() {
    const dialogRef = this.dialog.open(CreaeBuildingComponent, {
      // data: {
      //   id,
      // }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuilding();
      console.log('The dialog was closed', result);
    });
  }

  showItemList(id) {
    const dialogRef = this.dialog.open(ShowBuildingListComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuilding();
      console.log('The dialog was closed', result);
    });
  }

  onclickEdit(id) {
    const dialogRef = this.dialog.open(EditBuildingComponent, {
      // panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllBuilding();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete(id) {
    const dialogRef = this.dialog.open(DeleteBuildingComponent, {
      data: {
        id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllBuilding();
    });
  }

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllBuilding();
  }

  getAllBuilding() {
    this.SchoolPropertyService.getAllBuilding(this.paginate).subscribe(
      (data) => {
        console.log('data', data);

        var result: any = data;

        this.getAllData = result.data.data;
        this.paginate.totalCount = result.data.totalCount;
        this.datasoure = new MatTableDataSource<any>(this.getAllData);
        this.datasoure.data = this.getAllData;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllBuilding()
      : this.paginator.firstPage();
  }
}

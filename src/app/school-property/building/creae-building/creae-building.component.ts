import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { SchoolPropertyService } from '../../shared/school-property.service';

@Component({
  selector: 'app-creae-building',
  templateUrl: './creae-building.component.html',
  styleUrls: ['./creae-building.component.scss'],
})
export class CreaeBuildingComponent implements OnInit {
  createform: FormGroup;

  constructor(
    private formgroup: FormBuilder,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService,
    public dialogRef: MatDialogRef<CreaeBuildingComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {}

  ngOnInit(): void {
    this.createform = this.formgroup.group({
      room_name: ['', Validators.required],
      room_description: ['', Validators.required],
      room_floor: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const data = {
        room_name: this.createform.get('room_name').value,
        room_description: this.createform.get('room_description').value,
        room_floor: this.createform.get('room_floor').value,
      };

      console.log(data);

      this.SchoolPropertyService.createBuilding(data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Registered');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

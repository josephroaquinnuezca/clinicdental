import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay, filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LogoutComponent } from 'src/app/_reusesable-dialogs/logout/logout.component';
import { ProfileComponent } from 'src/app/_reusesable-dialogs/profile/profile.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  panelOpenState = false;
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  values: any;

  constructor(private observer: BreakpointObserver, public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }

  ngOnInit(): void {
    this.values = JSON.parse(localStorage.getItem('dataSource'));
  }

  openDialogLogout(): void {
    const dialogRef = this.dialog.open(LogoutComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }

  openProfileDialog(): void {
    const dialogRef = this.dialog.open(ProfileComponent, {
      // data: {name: this.name, animal: this.animal}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }
}

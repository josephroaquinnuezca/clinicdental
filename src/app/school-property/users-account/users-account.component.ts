import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SchoolPropertyService } from '../shared/school-property.service';
import { UsersCreateComponent } from './users-create/users-create.component';
import { UsersEditComponent } from './users-edit/users-edit.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users-account',
  templateUrl: './users-account.component.html',
  styleUrls: ['./users-account.component.scss'],
})
export class UsersAccountComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;

  isChecked: boolean = false;

  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

  searchString: String = '';
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  datasoure: MatTableDataSource<any>;
  getAllData: any = [];

  displaycolumn: string[] = [
    'number',
    'full_name',
    'contact',
    'date',
    'role',
    'status',
    'controls',
  ];

  constructor(
    public dialog: MatDialog,
    private toastr: ToastrService,
    public SchoolPropertyService: SchoolPropertyService
  ) {}

  ngOnInit() {
    this.getAllUsers();
  }

  onToggleChange() {
    // Handle the toggle change event here
    console.log('Toggle switched:', this.isChecked);
  }

  createRoom() {
    const dialogRef = this.dialog.open(UsersCreateComponent, {
      // data: {
      //   id,
      // }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllUsers();
      console.log('The dialog was closed', result);
    });
  }

  onclickEdit(id) {
    const dialogRef = this.dialog.open(UsersEditComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllUsers();
      console.log('The dialog was closed', result);
    });
  }

  onclickDelete() {}

  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllUsers();
  }

  getAllUsers() {
    this.SchoolPropertyService.getAllUsers(this.paginate).subscribe((data) => {
      console.log('data', data);

      var result: any = data;

      this.getAllData = result.data.data;
      this.paginate.totalCount = result.data.totalCount;
      this.datasoure = new MatTableDataSource<any>(this.getAllData);
      this.datasoure.data = this.getAllData;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllUsers()
      : this.paginator.firstPage();
  }

  onSlideToggleChange(event: any, id) {
    console.log('Slide toggle changed:', event.checked);
    const data = event.checked;
    if (event.checked == true) {
      this.SchoolPropertyService.updateUserStatus(id, data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.getAllUsers();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    } else {
      this.SchoolPropertyService.updateUserStatus(id, data).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.getAllUsers();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }
}

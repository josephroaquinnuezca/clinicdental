import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { SchoolPropertyService } from '../../shared/school-property.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss'],
})
export class UsersEditComponent implements OnInit {
  mobNumberPattern = '^((\\+91-?)|0)?[0-9]{10}$';

  // please import the FormGroup Module first
  hide = true;
  createform: FormGroup;
  submitted = false;

  minimumdate: any;

  myDate = new Date(Date.now());

  // minDate: moment.Moment;
  id: any;

  constructor(
    private router: Router,
    public SchoolPropertyService: SchoolPropertyService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    public dialogRef: MatDialogRef<UsersEditComponent>,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.id = results;
  }

  ngOnInit(): void {
    this.minimumdate = new Date();

    this.CreateFormValidation();
    this.getSingleUsers();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  CreateFormValidation() {
    this.createform = this.formgroup.group(
      {
        username: ['', Validators.required],
        fname: ['', Validators.required],
        mname: [''],
        lname: ['', Validators.required],
        email_add: ['', [Validators.required, Validators.email]],
        contact_no: [
          '',
          [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            Validators.minLength(11),
            Validators.maxLength(11),
          ],
        ],
        address: ['', Validators.required],
        // password: [null, Validators.required],
        // re_enterpass: ['', Validators.required],
      }
      // { validator: this.checkIfMatchingPasswords('password', 're_enterpass') }
    );
  }

  checkIfMatchingPasswords(
    passwordKey: string,
    passwordConfirmationKey: string
  ) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  get f() {
    return this.createform.controls;
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.createform.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onSubmit() {
    if (this.createform.invalid) {
      this.createform.markAllAsTouched();
      this.toastr.error('Please Complete all required Fields!');
      return;
    } else {
      const date = moment(this.myDate).format('l');
      const data = {
        fname: this.createform.get('fname').value,
        mname: this.createform.get('mname').value,
        lname: this.createform.get('lname').value,
        dob: '',
        gender: '',
        username: this.createform.get('username').value,
        // password: this.createform.get('password').value,
        contact_no: this.createform.get('contact_no').value,
        age: '',
        email: this.createform.get('email_add').value,
        address: this.createform.get('address').value,
        date_created: date,
      };

      this.SchoolPropertyService.updateUsers(data, this.id.id).subscribe(
        (data) => {
          console.log(data);
          this.toastr.success('Successfully Updated');
          this.dialogRef.close();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }

  getSingleUsers() {
    this.SchoolPropertyService.getSingleUsers(this.id.id).subscribe((data) => {
      console.log('data', data);
      var result: any = data;
      this.createform.get('username').patchValue(result.data.username);
      this.createform.get('fname').patchValue(result.data.fname);
      this.createform.get('mname').patchValue(result.data.mname);
      this.createform.get('lname').patchValue(result.data.lname);
      this.createform.get('email_add').patchValue(result.data.email);
      this.createform.get('contact_no').patchValue(result.data.contact_no);
      this.createform.get('address').patchValue(result.data.address);
    });
  }
}

import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP_API_URL_PUBLIC } from '../api-service/api-service.service';
import { retry, catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(
    private http: HttpClient,
  ) { }


  login(username: string, password: string){
    let API_URL = `${HTTP_API_URL_PUBLIC}login`;
    return this.http.post(API_URL, { username, password })


  }

  logout(): Observable<any>{
    return this.http.get(HTTP_API_URL_PUBLIC + '/logout')
    }


  // getError(error) {
  //   let message = '';
  //   if (error.error instanceof ErrorEvent) {
  //       // handle client-side errors
  //       message = `Error: ${error.error.message}`;
  //   } else {
  //       // handle server-side errors
  //       message = `Error Code: ${error.status}\nMessage: ${error.message}`;
  //   }
  //   console.log(message);
  //   return throwError(message);
  // }

}


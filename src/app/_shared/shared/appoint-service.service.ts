import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnyObject } from 'chart.js/types/basic';
import { Observable, throwError } from 'rxjs';
import { HTTP_API_URL } from '../api-service/api-service.service';

@Injectable({
  providedIn: 'root',
})
export class AppointServiceService {
  constructor(private http: HttpClient) {}


  //22-03/2023
  UpdateSingleAppointment(data: any, id) {
    let API_URL = `${HTTP_API_URL}update-single-appointment/` + id;
    return this.http.patch(API_URL, data);
  }
  //

  getAllAppointment(data){
    return this.http.get(`${HTTP_API_URL}get-all-appointment` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  getEditSingAppointment(id) {
    return this.http.get(`${HTTP_API_URL}edit-single-appointment/` + id);
  }

  getSingleAppointment(data) {
    let API_URL = `${HTTP_API_URL}get-single-appointment`;
    return this.http.post(API_URL, data);
  }


  addNoteInTeeth(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-note-teeth`;
    return this.http.post(API_URL, data);
  }


  createClientAppointment(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-client-appointment`;
    return this.http.post(API_URL, data);
  }

  getAllTransaction(data, id){
    return this.http.get(`${HTTP_API_URL}get-all-transaction` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&searchString=${id}`);
  }

  getSingleClientAppointment(id): Observable<any> {
    const url: string =
      `${HTTP_API_URL}get-client-appointment/` + `?id=${id}` ;
    return this.http.get<any>(url);
  }

  // + `?date=${date}`

  getAllClientAppointment(data:any,id) {
    return this.http.get(`${HTTP_API_URL}get-all-client-appointment/` + id  +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  DeleteSingletAppointment(id) {
    return this.http.delete(`${HTTP_API_URL}delete-single-appointment/` + id);
  }

  UpdateAlldateAppointment(date) {
    return this.http.get(
      `${HTTP_API_URL}update-all-appointment/` + `?date=${date}`
    );
    // let API_URL = `${HTTP_API_URL}update-all-appointment/` + date;
    // return this.http.patch(API_URL, formdata)
  }

  getSingleClientAppointmentAdmin(data): Observable<any> {
    return this.http.get(`${HTTP_API_URL}get-all-client-appointment-admin` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  GetAllClientAppointmentToday(date: any): Observable<any> {
    const url: string =
      `${HTTP_API_URL}get-all-client-appointment-today` + `?date=${date}`;
    return this.http.get<any>(url);
  }

  //MANAGE APPOINTMENT ADMIN

  createAppointment(data: any): Observable<any> {
    let API_URL = `${HTTP_API_URL}create-appointment`;
    return this.http.post(API_URL, data);
  }

  updateAppointmentAdmin(data: any, id): Observable<any> {
    let API_URL = `${HTTP_API_URL}update-appointment-admin/` + `?id=${id}`;
    return this.http.post(API_URL, data);
  }

  UpdateSingleWaitingClient(data:any) {
    return this.http.get(
      `${HTTP_API_URL}update-single-waiting-appointment/` + `?id=${data.id}&clientid=${data.clientid}`
    );
  }

  rejectAppointmentClient(id, contactno, comment) {

    let API_URL = `${HTTP_API_URL}reject-client-appointment/` + `?id=${id}&contactno=${contactno}`;
    return this.http.patch(API_URL, comment);
    // return this.http.pathc(
    //   `${HTTP_API_URL}reject-client-appointment/` + `?id=${id}&contactno=${contactno}`
    // );
  }

  //APPOINT HISTORY
  updateUserAppointment(data: any, id) {
    let API_URL = `${HTTP_API_URL}update-single-user-appointment/` + id;
    return this.http.patch(API_URL, data);
  }

  getSingleaAppointmentUser(id:any) {
    return this.http.get(`${HTTP_API_URL}get-single-appointment-user/` + id);
  }

  fetchmecalendar(): Observable<any> {
    const url: string = `${HTTP_API_URL}fetch-all-client-appointment-calendar`;
    return this.http.get<any>(url);
  }


  GetAllReportUsersList(date): Observable<any> {
    const url: string = `${HTTP_API_URL}get-all-report-users-list` + `?date=${date}`;
    return this.http.get<any>(url);
  }

  gellAllperMonthAppointment(month): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-per-monthly-appointment` + `?date=${month}`);
  }

  gellAllperYearAppointment(month): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-per-year-appointment` + `?date=${month}`);
  }


  GetSingleUserTeeth(id): Observable<any> {
    const url: string = `${HTTP_API_URL}get-single-user-teeth` + `?id=${id}`;
    return this.http.get<any>(url);
  }


  //validation of appointment per day
  validationDateToday(date: any): Observable<any> {
    const url: string =
      `${HTTP_API_URL}get-all-client-appointment-today` + `?date=${date}`;
    return this.http.get<any>(url);
  }
  //29-04-2023


  //11-06-2023
  appointentDateValidation(date: any, id:any): Observable<any> {
    const url: string =
      `${HTTP_API_URL}appointment-date-validation` + `?date=${date}&id=${id}`;
    return this.http.get<any>(url);
  }

  appointmentTimeValidation(time: any, id:any): Observable<any> {
    const url: string =
      `${HTTP_API_URL}appointment-time-validation` + `?time=${time}&id=${id}`;
    return this.http.get<any>(url);
  }



  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HTTP_API_URL } from '../api-service/api-service.service';

@Injectable({
  providedIn: 'root'
})
export class ClientServiceService {

  constructor(
    private http: HttpClient
  ) { }




  //transaction
  createTransaction(create: any): Observable<any>{
    let API_URL = `${HTTP_API_URL}create-transaction`;
    return this.http.post(API_URL, create)
  }

  getAllTransaction(data){
    return this.http.get(`${HTTP_API_URL}get-all-transaction` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }


  getSingleTransactionHistory(data:any, id:any){
    return this.http.get(`${HTTP_API_URL}get-single-transaction-hisotry` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&id=${id}`);
  }



  //services
  createservice(create: any): Observable<any>{
    let API_URL = `${HTTP_API_URL}create-service`;
    return this.http.post(API_URL, create)
  }

  getAllService(data:any) {
    return this.http.get(`${HTTP_API_URL}get-all-service` + `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }


  getSingleService(id:any): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-single-service` + `?id=${id}`);
  }

  updateService(data, id) {
      let API_URL = `${HTTP_API_URL}update-all-service/`  + `?id=${id}`;
      return this.http.patch(API_URL, data)

    // let API_URL = `${HTTP_API_URL}update-all-appointment/` + date;
    // return this.http.patch(API_URL, formdata)
  }

  //

  getAllClient(data:any) {
    return this.http.get(`${HTTP_API_URL}get-all-client` + `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  getSingleClient(id) {
    return this.http.get(`${HTTP_API_URL}get-single-client/${id}`);
  }


  createClient(create: any): Observable<any>{
    let API_URL = `${HTTP_API_URL}create-client`;
    return this.http.post(API_URL, create)
  }

  getalllCount(): Observable<any>{
    return this.http.get(`${HTTP_API_URL}get-all-count`);
  }


  getalllCountBydate(date): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-count-by-date` + `?date=${date}`);
  }

  getalllUsersBydate(date): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-users-by-date` + `?date=${date}`);
  }


  updateClient(update: any, id): Observable<any>{
    let API_URL = `${HTTP_API_URL}update-client/${id}`;
    return this.http.patch(API_URL, update)
  }

  ConfirmedClient(id){
    return this.http.get(`${HTTP_API_URL}confirmed-updated-appointment/` + `?id=${id}`);
  }


  DeleteClient(id){
    return this.http.get(`${HTTP_API_URL}delete-client-information/` + `?id=${id}`);
  }

  RejectClient(id){
    return this.http.get(`${HTTP_API_URL}reject-client-information/` + `?id=${id}`);
  }

  //weekly report
  gellAllperWeekUser(startdate, enddate): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-per-week-users` + `?startdate=${startdate}&enddate=${enddate}`);
  }

  gellAllperMonhlyUser(monthly): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-per-monthly-users` + `?date=${monthly}`);
  }

  gellAllperYearUser(year): Observable <any>{
    return this.http.get(`${HTTP_API_URL}get-all-per-yearly-users` + `?date=${year}`);
  }




  // createAccount(create:any): Observable<any>{
  //   let API_URL = `${HTTP_API_URL}create-account`;
  //   return this.http.post(API_URL, create)
  // }




   // Handle Errors
   error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}

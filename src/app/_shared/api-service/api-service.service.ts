import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiServiceService {
  constructor() {}
}

export const HTTP_API_URL =
  'https://dental-clinic-ucu-website-app-api.onrender.com/v1/private/users/';
export const HTTP_API_URL_PUBLIC =
  'https://dental-clinic-ucu-website-app-api.onrender.com/v1/public/users/';

// export const HTTP_API_URL = "http://localhost:4100/v1/private/users/";
// export const HTTP_API_URL_PUBLIC = "http://localhost:4100/v1/public/users/";
